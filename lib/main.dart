
// import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:firebase_analytics/observer.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/bindings/home_binding.dart';
import 'package:news_application/app/data/preference.dart';
import 'package:news_application/app/routes/app_pages.dart';
import 'package:news_application/app/ui/theme/app_theme.dart';
// import 'package:native_admob_flutter/native_admob_flutter.dart' as ads;

import 'app/data/remote/rest_client.dart';
import 'app/routes/app_routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await MobileAds.instance.initialize();
  await GetStorage.init();
  // await Firebase.initializeApp();
  // await ads.MobileAds.initialize(
  //   nativeAdUnitId: nativeAdUnitId,
  //   bannerAdUnitId: bannerAdUnitId,
  //   interstitialAdUnitId: interstitialAdUnitId,
  // );
  // FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  // await FirebaseCrashlytics.instance
  //     .setCrashlyticsCollectionEnabled(!kDebugMode); //false ปิด
  Get.put(Preference());
  Get.put(RestClient());

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);


  final screen = ScreenUtilInit( builder: (context, child) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          initialBinding: HomeBinding(),
          initialRoute: Routes.INITIAL,
          getPages: AppPages.pages,
          theme: appThemeData,
          // navigatorObservers: [FirebaseAnalyticsObserver(analytics: analytics)],
          locale: Locale('th', 'TH'),
          builder: EasyLoading.init(),
          title: 'News',
        );
  });
  runApp(screen);

}