

abstract class Routes {
  Routes._();
  static const INITIAL = '/';
  static const READ_NEWS = '/read_news';
  static const VIDEO_CONTENT = '/video_content';
  static const FEED_VIDEO = '/feed_video';
  static const FEED_TV = '/feed_tv';
  static const NEWS_PAPER = '/news_paper';
}