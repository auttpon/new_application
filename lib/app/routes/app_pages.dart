import 'package:get/get.dart';
import 'package:news_application/app/bindings/NewsPaperBinding.dart';
import 'package:news_application/app/bindings/feed_gold_binding.dart';
import 'package:news_application/app/bindings/feed_video_binding.dart';
import 'package:news_application/app/bindings/home_binding.dart';
import 'package:news_application/app/bindings/read_news_binding.dart';
import 'package:news_application/app/bindings/tv_online_binding.dart';
import 'package:news_application/app/bindings/video_content_binding.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/design/dashboard_ui.dart';
import 'package:news_application/app/ui/design/news_paper_ui.dart';
import 'package:news_application/app/ui/design/read_news_ui.dart';
import 'package:news_application/app/ui/design/tv_online_ui.dart';
import 'package:news_application/app/ui/design/video_content_ui.dart';


class AppPages {
  static final pages = [
    GetPage(
        name: Routes.INITIAL,
        page: () => DashboardUI(),
        bindings: [HomeBinding(), FeedVideoBinding(), FeedGoldBinding()]),
    GetPage(
        name: Routes.READ_NEWS,
        page: () => ReadNewsUI(),
        binding: ReadNewsBinding()),
    GetPage(
        name: Routes.VIDEO_CONTENT,
        page: () => VideoContentUI(),
        binding: VideoContentBinding()),
    GetPage(
      name: Routes.FEED_TV,
      page: ()=> TvOnlineUI(),
      binding: TvOnlineBinding()
    ),
    GetPage(
      name: Routes.NEWS_PAPER,
      page: ()=> NewsPaperUI(),
      binding: NewsPaperBinding()
    )
  ];
}
