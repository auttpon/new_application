import 'dart:convert';

import 'package:html/dom.dart';
import 'package:html/dom_parsing.dart';
import 'package:html/parser.dart';
import 'package:news_application/app/data/model/feed_gold.dart';
import 'package:news_application/app/data/model/feed_news_paper.dart';
import 'package:news_application/app/data/model/feed_tv.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/model/news_story.dart';
import 'package:news_application/app/data/model/read_news.dart';
import 'package:news_application/app/data/model/video_content.dart';

class ParseData {
  Map<String, dynamic>? _toJson(String html) {
    final doc = parse(html);
    final elementJson = doc.getElementById("__NEXT_DATA__");
    if (elementJson != null) {
      return jsonDecode(elementJson.text);
    }
    return null;
  }

  NewsColumn? toNewsColumn(String html) {
    final json = _toJson(html);
    return json == null ? null : NewsColumn.fromJson(json);
  }

  NewsStory? toNewsStory(String html) {
    final json = _toJson(html);
    if (json != null) {
      var data = json
          .getMapData('props')
          .getMapData('initialState')
          .getMapData('content')
          .getMapData('data');

      final readNews = ReadNews.fromJson(data);
      final docContent = parse(readNews.items?.content);
      docContent.getElementsByTagName('script').forEach((element) {
        element.remove();
      });

      final contentParse = docContent.body?.nodes
          .map((node) => NewsContentParse()..visit(node))
          .where((element) => element.story.length > 0)
          .toList();
      final story = <NewsStoryState>[];
      contentParse?.forEach((element) {
        story.addAll(element.story);
      });
      return NewsStory(readNews, story);
    }
    return null;
  }

  VideoContent? toVideoContent(String html) {
    final json = _toJson(html);
    if (json != null) {
      var initialState = json.getMapData('props').getMapData('initialState');
      var videoData = initialState.getMapData('video')['data']; //data as List
      var tvData = initialState.getMapData('tv')['data']; //data as List

      if (videoData is Map<String, dynamic>) {
        // final items = videoData["items"] as Map<String, dynamic>;
        final items = videoData.getMapData("items");
        return VideoContent.fromJson(items);
      } else if (tvData is Map<String, dynamic>) {
        // final items = tvData["items"] as Map<String, dynamic>;
        final items = tvData.getMapData("items");
        return VideoContent.fromJson(items);
      }
    }
    return null;
  }

  FeedVideo? toFeedVideo(String html) {
    final json = _toJson(html);
    if (json != null) {
      var items = json
          .getMapData('props')
          .getMapData('initialState')
          .getMapData('video')
          .getMapData('data')
          .getMapData('items');

      return FeedVideo.fromJson(items);
    }
    return null;
  }

  FeedGold? toFeedGold(String html) {
    final json = _toJson(html);
    if (json != null) {
      final items = json
          .getMapData('props')
          .getMapData('initialState')
          .getMapData('gold')
          .getMapData('data')
          .getMapData('items');
      return FeedGold.fromJson(items);
    }
    return null;
  }

  FeedTv? toFeedTv(String html){
    final json = _toJson(html);
    if (json != null) {
      final items = json
          .getMapData('props')
          .getMapData('initialState')
          .getMapData('tv')
          .getMapData('data')
          .getMapData('items');

      final doc = parse(items['live']);
      var tag = doc.querySelector('video');
      var image = tag?.attributes['poster'];
      var sourceVideo = tag?.querySelector('source')?.attributes['src'];
      print("tv link: $sourceVideo");
      return FeedTv.fromJson(items)
        ..image = image
      ..sourceVideo = sourceVideo;

    }
  }

  FeedNewsPaper? toFeedNewsPaper(String html){
    var json = _toJson(html);
    if(json != null){
      final items = json
          .getMapData('props')
          .getMapData('initialState')
          .getMapData('newspaper')
          .getMapData('data')
          .getMapData('items');
      FeedNewsPaper? feedNewsPaper;
      if(items.containsKey('page1Mapping')){
        feedNewsPaper = FeedNewsPaper.fromJson(items.getMapData('page1Mapping'));
      }
      if(feedNewsPaper != null){
        feedNewsPaper.latest = NewsPaperColumn.fromJson(items.getMapData('latest'));
      }
      return feedNewsPaper;
    }
    return null;
  }

}

extension GetMapData on Map<String, dynamic> {
  Map<String, dynamic> getMapData(key) => this[key] !as Map<String, dynamic>;
}

class NewsContentParse extends TreeVisitor {
  List<NewsStoryState> _story;
  final StringBuffer _str;
  NewsContentParse(): _str = StringBuffer(), _story = [];

  List<NewsStoryState> get story => _story;

  @override
  String toString() => _str.toString();

  @override
  void visitChildren(Node node) {
    _str.write('[visitChildren]:${node.text}'); ///delete
  }

  @override
  void visitComment(Comment node) {
    final data = htmlSerializeEscape(node.data!);
    _str.write('[comment]:$data');
  }

  @override
  void visitDocument(Document node) {
    _str.write('[pre]');
    visitChildren(node);
    _str.write('[pre]');
  }

  @override
  void visitDocumentFragment(DocumentFragment node) {
    _str.write('visitDocumentFragment:${node.text}'); ///delete
  }

  @override
  void visitDocumentType(DocumentType node) {
    _str.write('[document type] ${node.name}');
  }

  @override
  void visitElement(Element node) {

    var picture = node.querySelector('figure > picture > img');
    var video = node.querySelector('video > source');
    if(picture != null){
      var src = picture.attributes['src'] ?? '';
      if(src.isNotEmpty){
        _story.add(NewsStoryState.image(src));
      }
      _str.write('picture: $src');
    }else if(video != null){
      var src = video.attributes['src'] ?? '';
      if(src.isNotEmpty){
        _story.add(NewsStoryState.video(src));
      }
      _str.write('video: $src');
    }else{
      var text = node.text;
      if(text.isNotEmpty){
        _story.add(NewsStoryState.text('${node.outerHtml}'));
      }
      _str.write('visitElement[${node.nodeType}]: $text');
    }
  }

  @override
  void visitNodeFallback(Node node) {
    _str.write('[visitNodeFallback]:${node.text}'); ///delete
  }

  @override
  void visitText(Text node) {
    var text = node.text.trim();
    if(text.isNotEmpty){
      _str.write('[visitText]');
      _str.write(node.text.trim());
      _str.write('[visitText]');
    }
  }

}