import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:news_application/app/data/model/news_column.dart';

class Preference {
  static final Preference to = Get.find<Preference>();

  final _store = GetStorage("news_store");

  setCacheHtml(String key, String html) async {
    await _store.write('html:$key', html);
  }

  String? getCacheHtml(String key) => _store.read<String?>('html:$key');

  double get scaleFont => _store.read('scale_font') ?? 0.0;
  set scaleFont(double value) => _store.write('scale_font', value);

  isShowAds() {
    int stampTime = _store.read<int>('long_time_ads') ?? 1;
    int time = DateTime.now().minute;
    // _store.write('long_time_ads', time);
    int currentTime = time - (stampTime);
    debugPrint('time: ${currentTime}');
    if(currentTime < 0 || currentTime > 4){
      _store.write('long_time_ads', time);
      return true;
    }
    return false;
  }
}
