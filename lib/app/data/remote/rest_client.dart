import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:news_application/app/data/model/feed_gold.dart';
import 'package:news_application/app/data/model/feed_news_paper.dart';
import 'package:news_application/app/data/model/feed_tv.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/model/news_story.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/data/parse_data.dart';
import 'package:news_application/app/data/preference.dart';
import 'package:retry/retry.dart';

enum Category { Local, Business, Foreign, Politic, Society, Crime, Tech }

extension CategoryExt on Category {
  static const Map<Category, String> values = {
    Category.Local: "news/local",
    Category.Business: "news/business",
    Category.Foreign: "news/foreign",
    Category.Politic: "news/politic",
    Category.Society: "news/society",
    Category.Crime: "news/crime",
    Category.Tech: "news/tech"
  };

  String get pathUrl => 'https://www.thairath.co.th/${values[this]}';
}

class RestClient {
  ParseData _parseStory = ParseData();

  // final trace = FirebasePerformance.instance.newTrace('network_request_data');

  static RestClient to = Get.find<RestClient>();

  Preference _preference = Preference.to;

  Future<http.Response> _getRetry(Uri url) async {
    return await /*RetryOptions(maxAttempts: 8).*/ retry(
      () {
        print('retry url: ${url.toString()}');
        return http.get(url).timeout(Duration(seconds: 5));
      },
      retryIf: (e) {
        print('retryIf url: ${e.toString()}');
        return e is SocketException || e is TimeoutException;
      },
    );
  }

  Stream<NewsColumn?> getSteamColumn(Category category) async* {
    var uri = Uri.parse("${category.pathUrl}");
    var cache = _preference.getCacheHtml(uri.toString());
    if (cache != null) {
      yield _parseStory.toNewsColumn(cache);
    }
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      _preference.setCacheHtml(uri.toString(), response.body);
      try {
        yield _parseStory.toNewsColumn(response.body);
      } catch (e) {
        e.printError();
      }
    } else {
      yield null;
    }
  }

  Future<NewsColumn?> getColumns(Category category) async {
    final uri = Uri.parse("${category.pathUrl}");
    // final response = await http.get(uri);
    final response = await _getRetry(uri);

    NewsColumn? newsColumn;
    if (response.statusCode == 200) {
      var body = response.body;
      _preference.setCacheHtml(uri.toString(), body);
      try {
        newsColumn = _parseStory.toNewsColumn(body);
      } catch (e) {
        e.printError();
      }
    } else {
      print("error code: ${response.statusCode}");
    }
    return newsColumn;
  }

  Future<NewsStory?> getReadNews(String url) async {
    // trace.start();
    debugPrint(url);
    final response = await _getRetry(Uri.parse(url));
    NewsStory? newsStory;
    if (response.statusCode == 200) {
      // trace.incrementMetric('read_new_cache_hit', 1);
      try {
        newsStory = _parseStory.toNewsStory(response.body);
      } catch (e) {
        e.printError();
      }
    } else {
      // trace.incrementMetric('read_new_cache_miss', 1);
    }
    // trace.stop();
    return newsStory;
  }

  Future<VideoContent?> getVideoContent(String url) async {
    // trace.start();
    final response = await _getRetry(Uri.parse(url));
    VideoContent? videoContent;
    if (response.statusCode == 200) {
      // trace.incrementMetric('video_content_cache_hit', 1);
      try {
        videoContent = _parseStory.toVideoContent(response.body);
      } catch (e) {
        e.printError();
      }
    } else {
      // trace.incrementMetric('video_content_cache_miss', 1);
    }
    // trace.stop();
    return videoContent;
  }

  Future<FeedVideo?> getFeedVideo(String url) async {
    // trace.start();
    final response = await _getRetry(Uri.parse(url));
    FeedVideo? feedVideo;
    if (response.statusCode == 200) {
      // trace.incrementMetric('feed_video_content_cache_hit', 1);
      try {
        feedVideo = _parseStory.toFeedVideo(response.body);
      } catch (e) {
        e.printError();
      }
    } else {
      // trace.incrementMetric('feed_video_content_cache_miss', 1);
    }
    // trace.stop();
    return feedVideo;
  }

  Future<FeedGold?> getFeedGold(String url) async {
    // trace.start();
    final response = await _getRetry(Uri.parse(url));
    FeedGold? feedGold;
    if (response.statusCode == 200) {
      // trace.incrementMetric('feed_gold_cache_hit', 1);
      try {
        feedGold = _parseStory.toFeedGold(response.body);
      } catch (e) {
        e.printError();
      }
    } else {
      // trace.incrementMetric('feed_gold_cache_miss', 1);
    }
    return feedGold;
  }

  Future<FeedTv?> getFeedTv(String url) async {
    final response = await _getRetry(Uri.parse(url));
    FeedTv? feedTv;
    if (response.statusCode == 200) {
      try {
        feedTv = _parseStory.toFeedTv(response.body);
      } catch (e) {
        e.printError();
      }
    }
    return feedTv;
  }

  Future<FeedNewsPaper?> getFeedNewsPaper(String url) async {
    final response = await _getRetry(Uri.parse(url));
    if (response.statusCode == 200) {
      try {
        return _parseStory.toFeedNewsPaper(response.body);
      } catch (e) {
        e.printError();
      }
    }
  }
}
