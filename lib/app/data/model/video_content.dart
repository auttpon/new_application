import 'package:json_annotation/json_annotation.dart';
import 'package:news_application/app/data/model/news_column.dart';

part 'video_content.g.dart';

@JsonSerializable()
class VideoContent {
  List<Video>? content;
  List<Video>? relate;

  VideoContent({this.content, this.relate});

  factory VideoContent.fromJson(Map<String, dynamic> json) =>
      _$VideoContentFromJson(json);

  Map<String, dynamic> toJson() => _$VideoContentToJson(this);

}

@JsonSerializable()
class Video extends News {
  dynamic? id;
  String? duration, source;
  List<VideoPlayList>? playList;

  Video({this.id, this.duration, this.source, this.playList});

  @JsonKey(ignore: true)
  String? get durationVideo => duration != null
      ? duration
      : (playList?.isNotEmpty == true ? playList?.first.duration : null);

  @JsonKey(ignore: true)
  String? get sourceVideo => source != null
      ? source
      : (playList?.isNotEmpty == true ? playList?.first.source : null);

  factory Video.fromJson(Map<String, dynamic> json) => _$VideoFromJson(json);

  Map<String, dynamic> toJson() => _$VideoToJson(this);
}

@JsonSerializable()
class VideoPlayList {
  dynamic? id;
  dynamic? youtubeId;
  String? source;
  String? duration;

  VideoPlayList({this.id, this.youtubeId, this.source, this.duration});

  factory VideoPlayList.fromJson(Map<String, dynamic> json) =>
      _$VideoPlayListFromJson(json);

  Map<String, dynamic> toJson() => _$VideoPlayListToJson(this);
}

@JsonSerializable()
class FeedVideo {
  List<Video>? panorama;
  List<Video>? news;
  @JsonKey(name: 'interviewtalk')
  List<Video>? interview;
  List<Video>? documentary;
  List<Video>? ent;
  List<Video>? variety;
  List<Video>? business;
  List<Video>? auto;

  FeedVideo(
      {this.panorama,
      this.news,
      this.interview,
      this.documentary,
      this.ent,
      this.variety,
      this.business,
      this.auto});

  factory FeedVideo.fromJson(Map<String, dynamic> json) =>
      _$FeedVideoFromJson(json);

  Map<String, dynamic> toJson() => _$FeedVideoToJson(this);
}
