import 'package:news_application/app/data/model/read_news.dart';

class NewsStory {

  NewsStory(this.readNews, this.story);

  ReadNews readNews;
  List<NewsStoryState> story;

  List<NewsRelates>? get newsRelates => readNews.items?.relates;

}

abstract class NewsStoryState {
  NewsStoryState._();

  factory NewsStoryState.image(String src) = StoryImage;
  factory NewsStoryState.text(String text) = StoryText;
  factory NewsStoryState.video(String src) = StoryVideo;

}


class StoryImage implements NewsStoryState {
  String src;
  StoryImage(this.src);//: super._();

}

class StoryText implements NewsStoryState{
  String text;
  StoryText(this.text);//: super._();
}

class StoryVideo implements NewsStoryState{
  String src;
  StoryVideo(this.src);//: super._();
}