import 'package:json_annotation/json_annotation.dart';
import 'package:news_application/app/data/model/video_content.dart';
part 'feed_tv.g.dart';

@JsonSerializable()
class FeedTv {

  String? image;
  String? sourceVideo;

  List<Schedule>? schedule;

  @JsonKey(name: 'lastestNews')
  List<Video>? lastNews;

  FeedTv({this.image,this.sourceVideo,this.schedule,this.lastNews});

  factory FeedTv.fromJson(Map<String, dynamic> json) =>
      _$FeedTvFromJson(json);

  Map<String, dynamic> toJson() => _$FeedTvToJson(this);


}

@JsonSerializable()
class Schedule {
  String? image,title,time,onAirTime;

  Schedule({this.image,this.title,this.onAirTime,this.time});

  factory Schedule.fromJson(Map<String, dynamic> json) =>
      _$ScheduleFromJson(json);

  Map<String, dynamic> toJson() => _$ScheduleToJson(this);
}