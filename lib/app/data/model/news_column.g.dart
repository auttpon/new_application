// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_column.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsColumn _$NewsColumnFromJson(Map<String, dynamic> json) {
  return NewsColumn(
    page: json['page'] as String?,
    props: json['props'] == null
        ? null
        : Props.fromJson(json['props'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$NewsColumnToJson(NewsColumn instance) =>
    <String, dynamic>{
      'page': instance.page,
      'props': instance.props,
    };

Props _$PropsFromJson(Map<String, dynamic> json) {
  return Props(
    initialState: json['initialState'] == null
        ? null
        : InitialState.fromJson(json['initialState'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PropsToJson(Props instance) => <String, dynamic>{
      'initialState': instance.initialState,
    };

InitialState _$InitialStateFromJson(Map<String, dynamic> json) {
  return InitialState(
    common: json['common'] == null
        ? null
        : Common.fromJson(json['common'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$InitialStateToJson(InitialState instance) =>
    <String, dynamic>{
      'common': instance.common,
    };

Common _$CommonFromJson(Map<String, dynamic> json) {
  return Common(
    data: json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CommonToJson(Common instance) => <String, dynamic>{
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    items: json['items'] == null
        ? null
        : NewsItems.fromJson(json['items'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'items': instance.items,
    };

NewsItems _$NewsItemsFromJson(Map<String, dynamic> json) {
  return NewsItems(
    panorama: (json['panorama'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    highlight: (json['highlight'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    scoop: (json['scoop'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    lastNews: (json['lastestNews'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    breakingNews: (json['breakingNews'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    popular: (json['popular'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    video: (json['video'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    column: (json['column'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NewsItemsToJson(NewsItems instance) => <String, dynamic>{
      'panorama': instance.panorama,
      'highlight': instance.highlight,
      'scoop': instance.scoop,
      'lastestNews': instance.lastNews,
      'breakingNews': instance.breakingNews,
      'popular': instance.popular,
      'video': instance.video,
      'column': instance.column,
    };

News _$NewsFromJson(Map<String, dynamic> json) {
  return News(
    id: json['id'],
    title: json['title'] as String?,
    abstract: json['abstract'] as String?,
    canonical: json['canonical'] as String?,
    image: json['image'] as String?,
    publishTimeTh: json['publishTimeTh'] as String?,
    topic: json['topic'] as String?,
  );
}

Map<String, dynamic> _$NewsToJson(News instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'abstract': instance.abstract,
      'canonical': instance.canonical,
      'image': instance.image,
      'publishTimeTh': instance.publishTimeTh,
      'topic': instance.topic,
    };
