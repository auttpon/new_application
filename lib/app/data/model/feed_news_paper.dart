import 'package:json_annotation/json_annotation.dart';
import 'package:news_application/app/data/model/news_column.dart';

part 'feed_news_paper.g.dart';

@JsonSerializable()
class FeedNewsPaper {
  String? coverMedia;
  List<NewsPaper>? items;
  String? dateNewspaper;
  NewsPaperColumn? latest;

  FeedNewsPaper({this.coverMedia, this.dateNewspaper, this.items, this.latest});

  factory FeedNewsPaper.fromJson(Map<String, dynamic> json) =>
      _$FeedNewsPaperFromJson(json);

  Map<String, dynamic> toJson() => _$FeedNewsPaperToJson(this);
}

@JsonSerializable()
class NewsPaper extends News {

  String? publishTime;

  NewsPaper({this.publishTime});

  factory NewsPaper.fromJson(Map<String, dynamic> json) =>
      _$NewsPaperFromJson(json);

  Map<String, dynamic> toJson() => _$NewsPaperToJson(this);


}

@JsonSerializable()
class NewsPaperColumn {
  List<NewsPaper>? items;

  NewsPaperColumn({this.items});

  factory NewsPaperColumn.fromJson(Map<String, dynamic> json) =>
      _$NewsPaperColumnFromJson(json);

  Map<String, dynamic> toJson() => _$NewsPaperColumnToJson(this);
}