// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_news_paper.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedNewsPaper _$FeedNewsPaperFromJson(Map<String, dynamic> json) {
  return FeedNewsPaper(
    coverMedia: json['coverMedia'] as String?,
    dateNewspaper: json['dateNewspaper'] as String?,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => NewsPaper.fromJson(e as Map<String, dynamic>))
        .toList(),
    latest: json['latest'] == null
        ? null
        : NewsPaperColumn.fromJson(json['latest'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$FeedNewsPaperToJson(FeedNewsPaper instance) =>
    <String, dynamic>{
      'coverMedia': instance.coverMedia,
      'items': instance.items,
      'dateNewspaper': instance.dateNewspaper,
      'latest': instance.latest,
    };

NewsPaper _$NewsPaperFromJson(Map<String, dynamic> json) {
  return NewsPaper(
    publishTime: json['publishTime'] as String?,
  )
    ..id = json['id']
    ..title = json['title'] as String?
    ..abstract = json['abstract'] as String?
    ..canonical = json['canonical'] as String?
    ..image = json['image'] as String?
    ..publishTimeTh = json['publishTimeTh'] as String?
    ..topic = json['topic'] as String?;
}

Map<String, dynamic> _$NewsPaperToJson(NewsPaper instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'abstract': instance.abstract,
      'canonical': instance.canonical,
      'image': instance.image,
      'publishTimeTh': instance.publishTimeTh,
      'topic': instance.topic,
      'publishTime': instance.publishTime,
    };

NewsPaperColumn _$NewsPaperColumnFromJson(Map<String, dynamic> json) {
  return NewsPaperColumn(
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => NewsPaper.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NewsPaperColumnToJson(NewsPaperColumn instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
