import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/parse_data.dart';

part 'feed_gold.g.dart';

@JsonSerializable()
class FeedGold {
  @JsonKey(name: 'lastestNews')
  List<News>? last;

  List<News>? column;

  @JsonKey(name: 'priceToDay')
  Map<String, dynamic>? mapPriceToDay;

  @JsonKey(name: 'listGold')
  Map<String, dynamic>? mapListGold;

  @JsonKey(name: 'graph')
  Map<String, dynamic>? mapGraph;

  Oil? oil;

  Currencies? currencies;

  @JsonKey(name: 'setstocks')
  Stocks? stocks;

  FeedGold(
      {this.stocks,
      this.oil,
      this.column,
      this.last,
      this.currencies,
      this.mapGraph,
      this.mapListGold,
      this.mapPriceToDay});

  factory FeedGold.fromJson(Map<String, dynamic> json) =>
      _$FeedGoldFromJson(json);

  Map<String, dynamic> toJson() => _$FeedGoldToJson(this);

  @JsonKey(ignore: true)
  List<GoldWrap> get goldPriceToDay => _getGoldWrap(mapPriceToDay);

  @JsonKey(ignore: true)
  List<GoldWrap> get listGolds => _getGoldWrap(mapListGold);

  @JsonKey(ignore: true)
  List<GoldWrap> get graphs {
    if (mapGraph != null) {
      var listGold = <GoldWrap>[];
      mapGraph!.forEach((key, value) {
        listGold.add(GoldWrap(key, Gold.fromJson(value)));
      });
      return listGold;
    }
    return [];
  }

  List<GoldWrap> _getGoldWrap(Map<String, dynamic>? data) {
    if (data != null) {
      var listGold = <GoldWrap>[];
      data.keys.forEach((key) {
        data.getMapData(key).forEach((keyTime, value) {
          listGold.add(GoldWrap(key, Gold.fromJson(value)));
        });
      });
      return listGold;
    }
    return [];
  }
}

class GoldWrap {
  String date;
  Gold gold;

  GoldWrap(this.date, this.gold);
}

@JsonSerializable()
class Gold {
  String? time;
  GoldItem? ref;
  GoldItem? jewel;

  Gold({this.time, this.jewel, this.ref});

  factory Gold.fromJson(Map<String, dynamic> json) => _$GoldFromJson(json);

  Map<String, dynamic> toJson() => _$GoldToJson(this);
}

@JsonSerializable()
class GoldItem {
  String? type, status;

  dynamic? buy, sell; // string or int,double

  int? buyChange, sellChange;

  GoldItem(
      {this.sell,
      this.buy,
      this.type,
      this.status,
      this.buyChange,
      this.sellChange});

  factory GoldItem.fromJson(Map<String, dynamic> json) =>
      _$GoldItemFromJson(json);

  Map<String, dynamic> toJson() => _$GoldItemToJson(this);
}

@JsonSerializable()
class Stocks {
  String? date;

  List<StocksItem>? items;

  Stocks({this.date, this.items});

  factory Stocks.fromJson(Map<String, dynamic> json) => _$StocksFromJson(json);

  Map<String, dynamic> toJson() => _$StocksToJson(this);
}

@JsonSerializable()
class StocksItem {
  String? name;
  double? prior;
  String? total;
  double? hight;
  double? low;
  double? change;

  StocksItem(
      {this.name, this.change, this.hight, this.low, this.prior, this.total});

  factory StocksItem.fromJson(Map<String, dynamic> json) =>
      _$StocksItemFromJson(json);

  Map<String, dynamic> toJson() => _$StocksItemToJson(this);
}

@JsonSerializable()
class Currencies {
  String? date;
  CurrenciesItem? item;

  Currencies({this.date, this.item});

  factory Currencies.fromJson(Map<String, dynamic> json) =>
      _$CurrenciesFromJson(json);

  Map<String, dynamic> toJson() => _$CurrenciesToJson(this);
}

@JsonSerializable()
class CurrenciesItem {
  String? name, buy, sell;

  CurrenciesItem({this.name, this.buy, this.sell});

  factory CurrenciesItem.fromJson(Map<String, dynamic> json) =>
      _$CurrenciesItemFromJson(json);

  Map<String, dynamic> toJson() => _$CurrenciesItemToJson(this);
}

@JsonSerializable()
class Oil {
  @JsonKey(name: 'green_plus_91')
  String? greenPlus91;

  @JsonKey(name: 'gasohol_95')
  String? gasohol95;

  @JsonKey(name: 'gasohol_91')
  String? gasohol91;

  @JsonKey(name: 'gasohol_e20')
  String? gasoholE20;

  @JsonKey(name: 'gasohol_e85')
  String? gasoholE85;

  @JsonKey(name: 'hi_premium_diesel_s')
  String? hiPremiumDieselS;

  @JsonKey(name: 'hi_diesel_b20_s')
  String? hiDieselB20S;

  @JsonKey(name: 'powerd')
  String? powerd;

  @JsonKey(name: 'ngv')
  String? ngv;

  String? date;

  Oil(
      {this.greenPlus91,
      this.gasohol95,
      this.gasohol91,
      this.gasoholE20,
      this.gasoholE85,
      this.hiDieselB20S,
      this.hiPremiumDieselS,
      this.ngv,
      this.powerd,
      this.date});

  factory Oil.fromJson(Map<String, dynamic> json) => _$OilFromJson(json);

  Map<String, dynamic> toJson() => _$OilToJson(this);
}
