// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_content.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoContent _$VideoContentFromJson(Map<String, dynamic> json) {
  return VideoContent(
    content: (json['content'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    relate: (json['relate'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$VideoContentToJson(VideoContent instance) =>
    <String, dynamic>{
      'content': instance.content,
      'relate': instance.relate,
    };

Video _$VideoFromJson(Map<String, dynamic> json) {
  return Video(
    id: json['id'],
    duration: json['duration'] as String?,
    source: json['source'] as String?,
    playList: (json['playList'] as List<dynamic>?)
        ?.map((e) => VideoPlayList.fromJson(e as Map<String, dynamic>))
        .toList(),
  )
    ..title = json['title'] as String?
    ..abstract = json['abstract'] as String?
    ..canonical = json['canonical'] as String?
    ..image = json['image'] as String?
    ..publishTimeTh = json['publishTimeTh'] as String?
    ..topic = json['topic'] as String?;
}

Map<String, dynamic> _$VideoToJson(Video instance) => <String, dynamic>{
      'title': instance.title,
      'abstract': instance.abstract,
      'canonical': instance.canonical,
      'image': instance.image,
      'publishTimeTh': instance.publishTimeTh,
      'topic': instance.topic,
      'id': instance.id,
      'duration': instance.duration,
      'source': instance.source,
      'playList': instance.playList,
    };

VideoPlayList _$VideoPlayListFromJson(Map<String, dynamic> json) {
  return VideoPlayList(
    id: json['id'],
    youtubeId: json['youtubeId'],
    source: json['source'] as String?,
    duration: json['duration'] as String?,
  );
}

Map<String, dynamic> _$VideoPlayListToJson(VideoPlayList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'youtubeId': instance.youtubeId,
      'source': instance.source,
      'duration': instance.duration,
    };

FeedVideo _$FeedVideoFromJson(Map<String, dynamic> json) {
  return FeedVideo(
    panorama: (json['panorama'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    news: (json['news'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    interview: (json['interviewtalk'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    documentary: (json['documentary'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    ent: (json['ent'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    variety: (json['variety'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    business: (json['business'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
    auto: (json['auto'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$FeedVideoToJson(FeedVideo instance) => <String, dynamic>{
      'panorama': instance.panorama,
      'news': instance.news,
      'interviewtalk': instance.interview,
      'documentary': instance.documentary,
      'ent': instance.ent,
      'variety': instance.variety,
      'business': instance.business,
      'auto': instance.auto,
    };
