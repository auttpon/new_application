// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_news.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadNews _$ReadNewsFromJson(Map<String, dynamic> json) {
  return ReadNews(
    items: json['items'] == null
        ? null
        : Story.fromJson(json['items'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ReadNewsToJson(ReadNews instance) => <String, dynamic>{
      'items': instance.items,
    };

Story _$StoryFromJson(Map<String, dynamic> json) {
  return Story(
    content: json['content'] as String?,
    relates: (json['relates'] as List<dynamic>?)
            ?.map((e) => NewsRelates.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
  )
    ..id = json['id']
    ..title = json['title'] as String?
    ..abstract = json['abstract'] as String?
    ..canonical = json['canonical'] as String?
    ..image = json['image'] as String?
    ..publishTimeTh = json['publishTimeTh'] as String?
    ..topic = json['topic'] as String?;
}

Map<String, dynamic> _$StoryToJson(Story instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'abstract': instance.abstract,
      'canonical': instance.canonical,
      'image': instance.image,
      'publishTimeTh': instance.publishTimeTh,
      'topic': instance.topic,
      'content': instance.content,
      'relates': instance.relates,
    };

NewsRelates _$NewsRelatesFromJson(Map<String, dynamic> json) {
  return NewsRelates(
    publishTimeTh: json['publishTimeTh'] as String?,
    image: json['image'] as String?,
    title: json['title'] as String?,
    id: json['id'] as int?,
    fullPath: json['fullPath'] as String?,
  );
}

Map<String, dynamic> _$NewsRelatesToJson(NewsRelates instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'image': instance.image,
      'publishTimeTh': instance.publishTimeTh,
      'fullPath': instance.fullPath,
    };
