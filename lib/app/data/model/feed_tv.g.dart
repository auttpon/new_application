// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_tv.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedTv _$FeedTvFromJson(Map<String, dynamic> json) {
  return FeedTv(
    image: json['image'] as String?,
    sourceVideo: json['sourceVideo'] as String?,
    schedule: (json['schedule'] as List<dynamic>?)
        ?.map((e) => Schedule.fromJson(e as Map<String, dynamic>))
        .toList(),
    lastNews: (json['lastestNews'] as List<dynamic>?)
        ?.map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$FeedTvToJson(FeedTv instance) => <String, dynamic>{
      'image': instance.image,
      'sourceVideo': instance.sourceVideo,
      'schedule': instance.schedule,
      'lastestNews': instance.lastNews,
    };

Schedule _$ScheduleFromJson(Map<String, dynamic> json) {
  return Schedule(
    image: json['image'] as String?,
    title: json['title'] as String?,
    onAirTime: json['onAirTime'] as String?,
    time: json['time'] as String?,
  );
}

Map<String, dynamic> _$ScheduleToJson(Schedule instance) => <String, dynamic>{
      'image': instance.image,
      'title': instance.title,
      'time': instance.time,
      'onAirTime': instance.onAirTime,
    };
