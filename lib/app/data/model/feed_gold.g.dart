// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_gold.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedGold _$FeedGoldFromJson(Map<String, dynamic> json) {
  return FeedGold(
    stocks: json['setstocks'] == null
        ? null
        : Stocks.fromJson(json['setstocks'] as Map<String, dynamic>),
    oil: json['oil'] == null
        ? null
        : Oil.fromJson(json['oil'] as Map<String, dynamic>),
    column: (json['column'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    last: (json['lastestNews'] as List<dynamic>?)
        ?.map((e) => News.fromJson(e as Map<String, dynamic>))
        .toList(),
    currencies: json['currencies'] == null
        ? null
        : Currencies.fromJson(json['currencies'] as Map<String, dynamic>),
    mapGraph: json['graph'] as Map<String, dynamic>?,
    mapListGold: json['listGold'] as Map<String, dynamic>?,
    mapPriceToDay: json['priceToDay'] as Map<String, dynamic>?,
  );
}

Map<String, dynamic> _$FeedGoldToJson(FeedGold instance) => <String, dynamic>{
      'lastestNews': instance.last,
      'column': instance.column,
      'priceToDay': instance.mapPriceToDay,
      'listGold': instance.mapListGold,
      'graph': instance.mapGraph,
      'oil': instance.oil,
      'currencies': instance.currencies,
      'setstocks': instance.stocks,
    };

Gold _$GoldFromJson(Map<String, dynamic> json) {
  return Gold(
    time: json['time'] as String?,
    jewel: json['jewel'] == null
        ? null
        : GoldItem.fromJson(json['jewel'] as Map<String, dynamic>),
    ref: json['ref'] == null
        ? null
        : GoldItem.fromJson(json['ref'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GoldToJson(Gold instance) => <String, dynamic>{
      'time': instance.time,
      'ref': instance.ref,
      'jewel': instance.jewel,
    };

GoldItem _$GoldItemFromJson(Map<String, dynamic> json) {
  return GoldItem(
    sell: json['sell'],
    buy: json['buy'],
    type: json['type'] as String?,
    status: json['status'] as String?,
    buyChange: json['buyChange'] as int?,
    sellChange: json['sellChange'] as int?,
  );
}

Map<String, dynamic> _$GoldItemToJson(GoldItem instance) => <String, dynamic>{
      'type': instance.type,
      'status': instance.status,
      'buy': instance.buy,
      'sell': instance.sell,
      'buyChange': instance.buyChange,
      'sellChange': instance.sellChange,
    };

Stocks _$StocksFromJson(Map<String, dynamic> json) {
  return Stocks(
    date: json['date'] as String?,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => StocksItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$StocksToJson(Stocks instance) => <String, dynamic>{
      'date': instance.date,
      'items': instance.items,
    };

StocksItem _$StocksItemFromJson(Map<String, dynamic> json) {
  return StocksItem(
    name: json['name'] as String?,
    change: (json['change'] as num?)?.toDouble(),
    hight: (json['hight'] as num?)?.toDouble(),
    low: (json['low'] as num?)?.toDouble(),
    prior: (json['prior'] as num?)?.toDouble(),
    total: json['total'] as String?,
  );
}

Map<String, dynamic> _$StocksItemToJson(StocksItem instance) =>
    <String, dynamic>{
      'name': instance.name,
      'prior': instance.prior,
      'total': instance.total,
      'hight': instance.hight,
      'low': instance.low,
      'change': instance.change,
    };

Currencies _$CurrenciesFromJson(Map<String, dynamic> json) {
  return Currencies(
    date: json['date'] as String?,
    item: json['item'] == null
        ? null
        : CurrenciesItem.fromJson(json['item'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CurrenciesToJson(Currencies instance) =>
    <String, dynamic>{
      'date': instance.date,
      'item': instance.item,
    };

CurrenciesItem _$CurrenciesItemFromJson(Map<String, dynamic> json) {
  return CurrenciesItem(
    name: json['name'] as String?,
    buy: json['buy'] as String?,
    sell: json['sell'] as String?,
  );
}

Map<String, dynamic> _$CurrenciesItemToJson(CurrenciesItem instance) =>
    <String, dynamic>{
      'name': instance.name,
      'buy': instance.buy,
      'sell': instance.sell,
    };

Oil _$OilFromJson(Map<String, dynamic> json) {
  return Oil(
    greenPlus91: json['green_plus_91'] as String?,
    gasohol95: json['gasohol_95'] as String?,
    gasohol91: json['gasohol_91'] as String?,
    gasoholE20: json['gasohol_e20'] as String?,
    gasoholE85: json['gasohol_e85'] as String?,
    hiDieselB20S: json['hi_diesel_b20_s'] as String?,
    hiPremiumDieselS: json['hi_premium_diesel_s'] as String?,
    ngv: json['ngv'] as String?,
    powerd: json['powerd'] as String?,
    date: json['date'] as String?,
  );
}

Map<String, dynamic> _$OilToJson(Oil instance) => <String, dynamic>{
      'green_plus_91': instance.greenPlus91,
      'gasohol_95': instance.gasohol95,
      'gasohol_91': instance.gasohol91,
      'gasohol_e20': instance.gasoholE20,
      'gasohol_e85': instance.gasoholE85,
      'hi_premium_diesel_s': instance.hiPremiumDieselS,
      'hi_diesel_b20_s': instance.hiDieselB20S,
      'powerd': instance.powerd,
      'ngv': instance.ngv,
      'date': instance.date,
    };
