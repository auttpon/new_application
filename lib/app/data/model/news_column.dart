import 'package:json_annotation/json_annotation.dart';

part 'news_column.g.dart';

@JsonSerializable() //run: flutter pub run build_runner build
class NewsColumn {
  NewsColumn({this.page, this.props});

  String? page;
  Props? props;

  factory NewsColumn.fromJson(Map<String, dynamic> json) =>
      _$NewsColumnFromJson(json);

  Map<String, dynamic> toJson() => _$NewsColumnToJson(this);
}

@JsonSerializable()
class Props {
  Props({this.initialState});

  InitialState? initialState;

  factory Props.fromJson(Map<String, dynamic> json) => _$PropsFromJson(json);

  Map<String, dynamic> toJson() => _$PropsToJson(this);
}

@JsonSerializable()
class InitialState {
  InitialState({this.common});

  Common? common;

  factory InitialState.fromJson(Map<String, dynamic> json) =>
      _$InitialStateFromJson(json);

  Map<String, dynamic> toJson() => _$InitialStateToJson(this);
}

@JsonSerializable()
class Common {
  Common({this.data});

  Data? data;

  factory Common.fromJson(Map<String, dynamic> json) => _$CommonFromJson(json);

  Map<String, dynamic> toJson() => _$CommonToJson(this);
}

@JsonSerializable()
class Data {
  Data({this.items});

  NewsItems? items;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class NewsItems {
  NewsItems(
      {this.panorama,
      this.highlight,
      this.scoop,
      this.lastNews,
      this.breakingNews,
      this.popular,
      this.video,
      this.column});

  List<News>? panorama;
  List<News>? highlight;
  List<News>? scoop;

  @JsonKey(name: 'lastestNews')
  List<News>? lastNews;

  List<News>? breakingNews;
  List<News>? popular;
  List<News>? video;
  List<News>? column;

  factory NewsItems.fromJson(Map<String, dynamic> json) =>
      _$NewsItemsFromJson(json);

  Map<String, dynamic> toJson() => _$NewsItemsToJson(this);
}

@JsonSerializable()
class News {
  News({
    this.id,
    this.title,
    this.abstract,
    this.canonical,
    this.image,
    this.publishTimeTh,
    this.topic,
  });

  dynamic? id;
  String? title, abstract, canonical, image, publishTimeTh, topic;
  // List<dynamic>? tags;

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);

  Map<String, dynamic> toJson() => _$NewsToJson(this);
}
