

import 'package:json_annotation/json_annotation.dart';
import 'package:news_application/app/data/model/news_column.dart';

part 'read_news.g.dart';

@JsonSerializable()
class ReadNews {
  Story? items;

  ReadNews({this.items});

  factory ReadNews.fromJson(Map<String, dynamic> json) =>
      _$ReadNewsFromJson(json);

  Map<String, dynamic> toJson() => _$ReadNewsToJson(this);
}

@JsonSerializable()
class Story extends News{
  String? content;

  @JsonKey(defaultValue: <NewsRelates>[])
  List<NewsRelates>? relates;

  Story({this.content,this.relates});

  factory Story.fromJson(Map<String, dynamic> json) =>
      _$StoryFromJson(json);

  Map<String, dynamic> toJson() => _$StoryToJson(this);

}

@JsonSerializable()
class NewsRelates {
  int? id;

  String? title,image,publishTimeTh,fullPath;

  NewsRelates({this.publishTimeTh,this.image,this.title,this.id,this.fullPath});

  factory NewsRelates.fromJson(Map<String, dynamic> json) =>
      _$NewsRelatesFromJson(json);

  Map<String, dynamic> toJson() => _$NewsRelatesToJson(this);

}