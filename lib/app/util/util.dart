
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';

Color getColorStatusGold(String status) {
  // var status = status ?? 'same';
  if (status == "up") {
    return const Color(0xff53fdd7);
  } else if (status == "down") {
    return const Color(0xffff5182);
  } else {
    return Colors.white;
  }
}

Icon getIconStatusGold(String status){
  debugPrint('status: $status');
  if (status == "up") {
    return Icon(
      CupertinoIcons.arrowtriangle_up_fill,
      color: getColorStatusGold(status),
      size: TextSizeNormal,
    );
  } else if (status == "down") {
    return Icon(
      CupertinoIcons.arrowtriangle_down_fill,
      color: getColorStatusGold(status),
      size: TextSizeNormal,
    );
  } else {
    return Icon(
      CupertinoIcons.chevron_up_chevron_down,
      color: getColorStatusGold(status),
      size: TextSizeNormal,
    );
  }
}