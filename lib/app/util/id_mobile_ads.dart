import 'dart:io';

import 'package:flutter/foundation.dart' show kDebugMode;

class IdMobileAds {
  IdMobileAds._();

  static final TestDevices = const ['33BE2250B43518CCDA7DE426D04EE231'];

  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return kDebugMode
          ? 'ca-app-pub-3940256099942544/6300978111'
          : 'ca-app-pub-2733981991403384/5351204266';
    } else if (Platform.isIOS) {
      return kDebugMode ? 'ca-app-pub-3940256099942544/2934735716' : '';
    }
    throw new UnsupportedError("Unsupported platform");
  }

  static String get interstitialAdUnitId {
    if (Platform.isAndroid) {
      return kDebugMode
          ? 'ca-app-pub-3940256099942544/1033173712'
          : 'ca-app-pub-2733981991403384/7211080848';
    } else if (Platform.isIOS) {
      return kDebugMode ? 'ca-app-pub-3940256099942544/4411468910' : '';
    }
    throw new UnsupportedError("Unsupported platform");
  }
}
