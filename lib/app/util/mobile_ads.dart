import 'package:flutter/foundation.dart' show kDebugMode;

//https://developers.google.com/admob/android/test-ads#sample%5C_ad%5C_units
String get nativeAdUnitId {
  /// Always test with test ads
  if (kDebugMode)
    return 'ca-app-pub-3940256099942544/2247696110';
  else return 'your-native-ad-unit-id';
}

String get bannerAdUnitId {
  /// Always test with test ads
  if (kDebugMode)
    return 'ca-app-pub-3940256099942544/6300978111';
  else return 'you-banner-ad-unit-id';
}

String get interstitialAdUnitId {
  /// Always test with test ads
  if (kDebugMode)
    return 'ca-app-pub-3940256099942544/1033173712';
  else return 'you-interstitial-ad-unit-id';
}

String get rewardedAdUnitId {
  /// Always test with test ads
  if (kDebugMode)
    return 'ca-app-pub-3940256099942544/5224354917';
  else return 'you-interstitial-ad-unit-id';
}

String get appOpenAdUnitId {
  /// Always test with test ads
  if (kDebugMode) return 'ca-app-pub-3940256099942544/3419835294';
  else return 'you-app-open-ad-unit-id';
}

String get rewardedInterstitialAdUnitId {
  /// Always test with test ads
  if (kDebugMode)
    return 'ca-app-pub-3940256099942544/5354046379';
  else return 'you-interstitial-ad-unit-id';
}