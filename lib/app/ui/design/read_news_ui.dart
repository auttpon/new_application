import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/controller/read_news_controller.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/model/news_story.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/design/photo_view_ui.dart';
import 'package:news_application/app/ui/design/video_content_ui.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/card_banner_image.dart';
import 'package:news_application/app/ui/widget/referent_link.dart';
import 'package:url_launcher/url_launcher.dart';

class ReadNewsUI extends GetView<ReadNewsController> {
  ScrollController _scrollController = ScrollController();

  static void open(News news) {
    Get.toNamed(Routes.READ_NEWS, arguments: {'news': news.toJson()});
  }

  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Get.back();
          },
        ),
        title: Text(
          "อ่านข่าว",
          style: TextStyle(fontSize: TextSizeBig, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.format_size),
            onPressed: (){
              controller.setFontSize();
            },
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          child: Obx(() => CustomScrollView(
                controller: _scrollController,
                slivers: [
                  SliverToBoxAdapter(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: InkWell(
                            onTap: () {
                              final src = controller.news?.image;
                              final tag =
                                  controller.news?.id.toString() ?? 'tag';
                              if (src != null) {
                                Get.to(PhotoViewUI(tag: tag, src: src,));
                              }
                            },
                            child: Container(
                              child: Hero(
                                tag: '${controller.news?.id}',
                                child: Image.network(
                                  '${controller.news?.image}',
                                  fit: BoxFit.cover,
                                ),
                              ),
                              // height: width * 2 / 3,
                              height: 0.5625.sw,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.w),
                          child: Text(
                            '${controller.news?.title}',
                            style: TextStyle(
                                fontSize: TextSizeMedium + controller.scaleFont,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 8.w, right: 8.w),
                          child: Text(
                            'วันที่ ${controller.news?.publishTimeTh}',
                            style: TextStyle(
                              fontSize: TextSizeNormal + controller.scaleFont,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8.w),
                          child: ReferentLink(
                            controller.news?.canonical ?? '',
                            alignment: MainAxisAlignment.start,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 8.w, right: 8.w),
                          child: Divider(
                            thickness: 1,
                            color: Colors.red[600],
                          ),
                        ),
                      ],
                    ),
                  ),
                  // SliverToBoxAdapter(
                  //   child: controller.isLoadAdsBanner ? Padding(
                  //     padding: EdgeInsets.all(8.w),
                  //     child: Container(
                  //       width: controller.bannerAd.size.width.toDouble(),
                  //       height: controller.bannerAd.size.height.toDouble(),
                  //       child: AdWidget(
                  //         ad: controller.bannerAd,
                  //       ),
                  //     ),
                  //   ) : SizedBox.shrink(),
                  // ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      final state = controller.newsStory[index];
                      if (state is StoryText) {
                        return Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.w,),
                          // child: Text(
                          //   state.text,
                          //   style: TextStyle(
                          //     fontSize: TextSizeMedium,
                          //   ),
                          // ),
                          child: Html(
                            data: state.text,
                            style: {
                              'p': Style(fontSize: FontSize(TextSizeNormal + controller.scaleFont))
                            },
                            onLinkTap: (url, _, __) async {
                              controller.clickLinkCanLaunch(url);
                            },
                          ),
                        );
                      } else if (state is StoryImage) {
                        return Padding(
                          padding: EdgeInsets.symmetric(vertical: 4.w),
                          child: InkWell(
                            onTap: () {
                              final src = state.src;
                              Get.to(PhotoViewUI(
                                tag: src,
                                src: src,
                              ));
                            },
                            child: Hero(
                              tag: '${state.src}',
                              child: Image.network(
                                '${state.src}',
                                // placeholder: "assets/icon/app_logo_2.png",
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        );
                      } else if (state is StoryVideo) {
                        return Container(
                          height: 0.5625.sw,
                          child: Align(
                            alignment: Alignment.center,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Image.network(
                                  '${controller.news?.image ?? ''}',
                                  fit: BoxFit.cover,
                                ),
                                IconButton(
                                  iconSize: 60.w,
                                  icon: Icon(
                                    Icons.play_circle_fill,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                    final video =
                                        controller.getVideo(state.src);
                                    VideoContentUI.open(
                                        'video', video.toJson());
                                  },
                                )
                              ],
                            ),
                          ),
                        );
                      } else {
                        return Text('-----unknown-----',
                            style: TextStyle(
                              fontSize: TextSizeMedium,
                            ));
                      }
                    }, childCount: controller.newsStory.length),
                  ),
                  SliverVisibility(
                    visible: controller.newsRelates.isNotEmpty,
                    sliver: SliverPadding(
                      padding: EdgeInsets.only(left: 8.w, right: 8.w),
                      sliver: SliverToBoxAdapter(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Divider(
                              thickness: 1,
                              color: Colors.red[600],
                            ),
                            Text(
                              'ข่าวที่เกี่ยวข้อง',
                              style: TextStyle(
                                  fontSize: TextSizeMedium,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SliverVisibility(
                    sliver: SliverToBoxAdapter(
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 4.0),
                        height: 130.w,
                        child: PageView.builder(
                          physics: const BouncingScrollPhysics(),
                          controller: PageController(viewportFraction: 0.83),
                          itemBuilder: (context, index) {
                            final relates = controller.newsRelates[index];
                            return InkWell(
                              onTap: () {
                                _scrollController.animateTo(
                                  0.0,
                                  curve: Curves.easeOut,
                                  duration: const Duration(milliseconds: 300),
                                );
                                controller.getNews(
                                    'https://www.thairath.co.th${relates.fullPath}');
                              },
                              child: CardBannerImage(
                                title: relates.title ?? '',
                                image: relates.image ?? '',
                              ),
                            );
                          },
                          // scrollDirection: Axis.horizontal,
                          itemCount: controller.newsRelates.length,
                        ),
                      ),
                    ),
                    visible: controller.newsRelates.isNotEmpty,
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
