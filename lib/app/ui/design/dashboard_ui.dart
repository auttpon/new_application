import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:news_application/app/ui/design/feed_gold_ui.dart';
import 'package:news_application/app/ui/design/feed_video_ui.dart';
import 'package:news_application/app/ui/design/history_ui.dart';
import 'package:news_application/app/ui/design/home_ui.dart';
import 'package:news_application/app/ui/design/news_paper_ui.dart';
import 'package:news_application/app/ui/design/tv_online_ui.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:package_info_plus/package_info_plus.dart';

class DashboardUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DashboardUI();
  }
}

class _DashboardUI extends State<DashboardUI> {
  int _selectedIndex = 0;
  bool isInit = false;
  final _widgetOptions = <Widget>[
    HomeUI(),
    FeedVideoUI(),
    FeedGoldUI(),
    HistoryUI(),
    // NativeAds()
  ];
  final _tabs = [
    GButton(
      icon: Icons.home,
      text: 'หน้าหลัก',
      textStyle: TextStyle(fontSize: TextSizeNormal),
    ),
    GButton(
      icon: Icons.slow_motion_video_outlined,
      text: 'วิดีโอ',
      textStyle: TextStyle(fontSize: TextSizeNormal),
    ),
    GButton(
      icon: Icons.bar_chart_outlined,
      text: 'ราคาทอง',
      textStyle: TextStyle(fontSize: TextSizeNormal),
    ),
    GButton(
      icon: Icons.history,
      text: 'ประวัติ',
      textStyle: TextStyle(fontSize: TextSizeNormal),
    ),
  ];

  String version = "-";

  @override
  void initState() {
    super.initState();
    _getPackageInfo();
  }

  _getPackageInfo() async {
    final platform = await PackageInfo.fromPlatform();
    setState(() {
      version = platform.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Future<String> _calculation = Future<String>.delayed(
      const Duration(seconds: 1),
      () => 'Data Loaded',
    );
    return FutureBuilder(
      future: _calculation,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting && !isInit) {
          return _splashScreen();
        } else {
          isInit = true;
          return _dashboard();
        }
      },
      initialData: Future.delayed(Duration(seconds: 1)),
    );
  }

  Widget _dashboard() {
    return Container(
      color: Colors.white,
      child: DefaultTabController(
        length: _tabs.length,
        initialIndex: _selectedIndex,
        child: SafeArea(
          bottom: false,
          child: Scaffold(
            endDrawerEnableOpenDragGesture: false,
            drawer: Drawer(
              child: _menu(),
            ),
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(120.w),
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text(
                    "News Application",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: TextSizeMedium),
                    // textAlign: TextAlign.end,
                  ),
                  subtitle: Text(
                    "version $version",
                    // textAlign: TextAlign.end,
                    style: TextStyle(fontSize: TextSizeNormal),
                  ),
                  leading: Builder(
                    builder: (_context) {
                      return InkWell(
                        onTap: () {
                          Scaffold.of(_context).openDrawer();
                        },
                        child: Container(
                          width: 45.w,
                          height: 45.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.r),
                            image: DecorationImage(
                              image: AssetImage("assets/icon/app_logo_2.png"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
            body: _widgetOptions.elementAt(_selectedIndex),
            // bottomNavigationBar: Container(
            //   decoration: BoxDecoration(color: Colors.white, boxShadow: [
            //     BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
            //   ]),
            //   child: Padding(
            //     padding: EdgeInsets.all(14.w),
            //     child: GNav(
            //       color: Colors.pink,
            //       rippleColor: Colors.pink,
            //       hoverColor: Color(0xFFFCE4EC),
            //       gap: 8,
            //       activeColor: Colors.pink,
            //       iconSize: 24,
            //       padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
            //       duration: Duration(milliseconds: 400),
            //       tabBackgroundColor: Color(0xFFFCE4EC),
            //       tabs: _tabs,
            //       selectedIndex: _selectedIndex,
            //       onTabChange: (index) {
            //         setState(() {
            //           _selectedIndex = index;
            //         });
            //       },
            //     ),
            //   ),
            // ),
          ),
        ),
      ),
    );
  }

  Widget _menu() {
    return ListView(
      // Important: Remove any padding from the ListView.
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          margin: EdgeInsets.zero,
          padding: EdgeInsets.zero,
          decoration: BoxDecoration(color: Color(0xFFF4A4A4A)),
          child: Image.asset(
            'assets/icon/app_logo_1.png',
            // height: MediaQuery.of(context).size.width * 2 / 3,
            fit: BoxFit.fitHeight,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'เมนู',
            style: TextStyle(fontSize: TextSizeMedium),
          ),
        ),
        Divider(
          height: 1,
          thickness: 1,
        ),
        SizedBox(
          height: 16.h,
        ),
        ListTile(
          leading: Icon(CupertinoIcons.news),
          title: Text(
            'ข่าวหน้าหนึ่ง',
            style: TextStyle(fontSize: TextSizeNormal),
          ),
          onTap: () {
            // Navigator.of(context).pop();
            Get.back();
            NewsPaperUI.open();
          },
        ),
        ListTile(
          leading: Icon(Icons.live_tv),
          title: Text(
            'ทีวีออนไลน์',
            style: TextStyle(fontSize: TextSizeNormal),
          ),
          onTap: () {
            Get.back();
            TvOnlineUI.open();
          },
        ),
        SizedBox(
          height: 16.h,
        ),
        Divider(
          height: 1,
          thickness: 1,
        ),
      ],
    );
  }

  Widget _splashScreen() {
    return Container(
      color: Colors.white, //0xFFF06292
      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
              width: 120.w,
              height: 120.h,
              child: Image.asset('assets/icon/app_logo_1.png')),
          Text(
            'News Application',
            style: TextStyle(fontSize: TextSizeBig),
          ),
          Text(
            "version $version",
            // textAlign: TextAlign.end,
            style: TextStyle(fontSize: TextSizeNormal),
          ),
        ],
      )),
    );
  }
}
