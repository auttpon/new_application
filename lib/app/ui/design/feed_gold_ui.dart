import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/controller/feed_gold_controller.dart';
import 'package:news_application/app/data/model/feed_gold.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/design/read_news_ui.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/gold_bar_chart.dart';
import 'package:news_application/app/ui/widget/referent_link.dart';
import 'package:news_application/app/ui/widget/secondary_card.dart';
import 'package:news_application/app/util/util.dart';

class FeedGoldUI extends GetView<FeedGoldController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(ScreenUtil().statusBarHeight + 8.w),
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: Row(
                children: [
                  Icon(Icons.monetization_on_outlined),
                  SizedBox(
                    width: 8.w,
                  ),
                  Center(
                      child: Text(
                    "ข่าว | ราคาทอง",
                    style: TextStyle(
                        fontSize: TextSizeMedium, fontWeight: FontWeight.bold),
                  )),
                ],
              ),
            ),
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.getFeedGold();
          },
          child: Obx(() => CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: [
              SliverToBoxAdapter(
                child: cardPriceToDay(),
              ),
              // SliverToBoxAdapter(
              //   child: controller.isLoadAdsBanner ? Padding(
              //     padding: EdgeInsets.all(8.w),
              //     child: Container(
              //       width: controller.bannerAd.size.width.toDouble(),
              //       height: controller.bannerAd.size.height.toDouble(),
              //       child: AdWidget(
              //         ad: controller.bannerAd,
              //       ),
              //     ),
              //   ) : SizedBox.shrink(),
              // ),
              SliverToBoxAdapter(
                  child: Padding(
                    padding:
                    EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                    child: Visibility(
                      visible: controller.news.isNotEmpty,
                      child: Text(
                        "ข่าวราคาทองคำ",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                      (context, index) {
                    final news = controller.news[index];
                    return InkWell(
                      onTap: () {
                        ReadNewsUI.open(news);
                      },
                      child: Container(
                          width: double.infinity,
                          height: 150.w,
                          margin: EdgeInsets.symmetric(horizontal: 8.w),
                          child: SecondaryCard(news)),
                    );
                  },
                  childCount: controller.news.length,
                ),
              ),
              SliverVisibility(
                visible: controller.originalLink.isNotEmpty,
                sliver: SliverToBoxAdapter(
                  child: ReferentLink(controller.originalLink),
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }

  Card cardPriceToDay() {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
      elevation: 2.0,
      color: const Color(0xff2c4260),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.r)),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 16.w, horizontal: 8.w),
        child: Column(
          children: [
            Text(
              "ราคาทองคำวันนี้",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: TextSizeMedium,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.all(8.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "วันที่ ${controller.priceToDay?.date ?? '-'}",
                          style: TextStyle(
                            fontSize: TextSizeNormal,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "ราคาอัพเดทล่าสุด ณ เวลา ${controller.priceToDay?.gold.time ?? '-'}",
                          style: TextStyle(
                            fontSize: TextSizeNormal,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      // Icon(
                      //   CupertinoIcons.arrowtriangle_up_fill,
                      //   color: getColorStatusGold(
                      //       controller.priceToDay?.gold.ref?.status ?? ''),
                      //   size: TextSizeNormal,
                      // ),
                      getIconStatusGold(controller.priceToDay?.gold.ref?.status ?? ''),
                      SizedBox(
                        width: 4.w,
                      ),
                      Text(
                        "${controller.priceToDay?.gold.ref?.sellChange?.toString() ?? '-'} บาท",
                        style: TextStyle(
                            fontSize: TextSizeMedium,
                            color: getColorStatusGold(
                                controller.priceToDay?.gold.ref?.status ?? ''),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                child: Text(
                  "ราคาทองคำแท่ง",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: TextSizeMedium,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาซื้อ(บาทละ)",
                        style: TextStyle(
                            color: Colors.white, fontSize: TextSizeNormal),
                      ),
                      Text(
                        "${controller.priceToDay?.gold.ref?.buy ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาขาย(บาทละ)",
                        style: TextStyle(
                            fontSize: TextSizeNormal, color: Colors.white),
                      ),
                      Text(
                        "${controller.priceToDay?.gold.ref?.sell ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                child: Text(
                  "ราคาทองรูปพรรณ",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: TextSizeMedium,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาซื้อ(บาทละ)",
                        style: TextStyle(
                            color: Colors.white, fontSize: TextSizeNormal),
                      ),
                      Text(
                        "${controller.priceToDay?.gold.jewel?.buy ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาขาย(บาทละ)",
                        style: TextStyle(
                            color: Colors.white, fontSize: TextSizeNormal),
                      ),
                      Text(
                        "${controller.priceToDay?.gold.jewel?.sell ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
