import 'package:flutter/material.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewUI extends StatelessWidget {
  String? src;
  String tag;
  String title;
  PhotoViewUI({this.tag = 'tag', @required this.src, this.title = ''});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(fontSize: TextSizeBig, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: PhotoView(
          tightMode: true,
          heroAttributes: PhotoViewHeroAttributes(tag: tag),
          imageProvider: NetworkImage('$src'),
          loadingBuilder: (context, event) {
            if (event == null) {
              return const Center(
                child: Text("Loading"),
              );
            }

            final value = event.cumulativeBytesLoaded /
                (event.expectedTotalBytes ?? event.cumulativeBytesLoaded);

            final percentage = (100 * value).floor();
            return Center(
              child: Text("$percentage%"),
            );
          },
        ),
      ),
    );
  }
}
