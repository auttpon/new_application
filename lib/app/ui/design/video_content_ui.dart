import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/controller/video_content_controller.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:news_application/app/ui/widget/referent_link.dart';
import 'package:news_application/app/ui/widget/video_card.dart';
import 'package:url_launcher/url_launcher.dart';

class VideoContentUI extends GetView<VideoContentController> {
  static void open(String key, Map<String, dynamic> json) {
    Get.toNamed(Routes.VIDEO_CONTENT, arguments: {key: json});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'วีดีโอ',
          style: TextStyle(fontSize: TextSizeBig, fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Obx(() => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  controller.chewieController != null &&
                          controller.chewieController!.videoPlayerController
                              .value.isInitialized
                      ? AspectRatio(
                          aspectRatio: controller.aspectRatio,
                          child: Chewie(
                            controller: controller.chewieController!,
                          ),
                        )
                      : Container(
                          height: 0.5625.sw,
                          child: Hero(
                            tag: '${controller.news?.id?.toString() ?? ''}',
                            child: Container(
                              child: Align(
                                alignment: Alignment.topCenter,
                                child: FadeInImage.assetNetwork(
                                  placeholder: "assets/icon/app_logo_2.png",
                                  image: '${controller.news?.image ?? ''}',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 0),
                    child: ReferentLink(
                      controller.news?.canonical ?? '',
                      alignment: MainAxisAlignment.start,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.w, right: 8.w),
                    child: Text(
                      'วันที่ ${controller.news?.publishTimeTh ?? 'unknown'}',
                      style: TextStyle(
                        fontSize: TextSizeNormal,
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                    child: Text(
                      '${controller.news?.title}',
                      style: TextStyle(
                          fontSize: TextSizeMedium,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                    child: Text(
                      '${controller.news?.abstract}',
                      style: TextStyle(fontSize: TextSizeNormal),
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  // Align(
                  //   alignment: Alignment.center,
                  //   child: controller.isLoadAdsBanner ? Padding(
                  //     padding: EdgeInsets.all(8.w),
                  //     child: Container(
                  //       width: controller.bannerAd.size.width.toDouble(),
                  //       height: controller.bannerAd.size.height.toDouble(),
                  //       child: AdWidget(
                  //         ad: controller.bannerAd,
                  //       ),
                  //     ),
                  //   ) : SizedBox.shrink(),
                  // ),
                  // Padding(
                  //   padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 0),
                  //   child: Divider(
                  //     thickness: 1,
                  //     color: Colors.pink[600],
                  //   ),
                  // ),

                  // Visibility(
                  //   visible: controller.relate.isNotEmpty,
                  //   child: Padding(
                  //     padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                  //     child: Text(
                  //       "วิดีโออื่นๆ",
                  //       style: TextStyle(fontSize: TextSizeNormal),
                  //     ),
                  //   ),
                  // ),
                  // Expanded(
                  //   child: GridView.builder(
                  //     itemBuilder: (context, index) {
                  //       final video = controller.relate[index];
                  //       return InkWell(
                  //           onTap: () async {
                  //             await controller.getVideoContent(video);
                  //           },
                  //           child: VideoCard(video));
                  //     },
                  //     itemCount: controller.relate.length,
                  //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //         crossAxisCount: 2, childAspectRatio: 1.72),
                  //   ),
                  // )
                ],
              )),
        ),
      ),
    );
  }
}
