import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:news_application/app/controller/feed_gold_controller.dart';
import 'package:news_application/app/data/model/feed_gold.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/referent_link.dart';
import 'package:news_application/app/util/util.dart';

class HistoryUI extends GetView<FeedGoldController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(ScreenUtil().statusBarHeight + 8.w),
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: Row(
              children: [
                Icon(Icons.history),
                SizedBox(
                  width: 8.w,
                ),
                Center(
                    child: Text(
                  "ราคาทองย้อนหลัง",
                  style: TextStyle(
                      fontSize: TextSizeMedium, fontWeight: FontWeight.bold),
                )),
              ],
            ),
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.getFeedGold();
        },
        child: Obx(()=> CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                var gold = controller.listGolds[index];
                return _createCardGold(gold);
              }, childCount: controller.listGolds.length),
            ),
            SliverVisibility(
              visible: controller.originalLink.isNotEmpty,
              sliver: SliverToBoxAdapter(
                child: ReferentLink(controller.originalLink),
              ),
            ),
          ],
        )),
      ),
    );
  }

  Card _createCardGold(GoldWrap goldWrap) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
      elevation: 2.0,
      color: const Color(0xff2c4260),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.r)),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 16.w, horizontal: 8.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 8.w,
              ),
              child: Row(
                children: [
                  Text(
                    "วันที่ ${goldWrap.date}",
                    style: TextStyle(
                        fontSize: TextSizeNormal,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 8.w,
                  ),
                  Icon(Icons.access_time, color: Colors.white, size: TextSizeNormal,),
                  SizedBox(
                    width: 4.w,
                  ),
                  Text(
                    "เวลา: ${goldWrap.gold.time}",
                    style: TextStyle(
                        fontSize: TextSizeNormal,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                    child: Text(
                      "ราคาทองคำแท่ง",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: TextSizeMedium,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Expanded(child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    getIconStatusGold(goldWrap.gold.ref?.status ?? ''),
                    SizedBox(
                      width: 4.w,
                    ),
                    Text(
                      "${goldWrap.gold.ref?.buyChange ?? '-'} บาท",
                      style: TextStyle(
                          fontSize: TextSizeMedium,
                          color: getColorStatusGold(goldWrap.gold.ref?.status ?? ''),
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาซื้อ(บาทละ)",
                        style: TextStyle(
                            color: Colors.white, fontSize: TextSizeNormal),
                      ),
                      Text(
                        "${goldWrap.gold.ref?.buy ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาขาย(บาทละ)",
                        style: TextStyle(
                            fontSize: TextSizeNormal, color: Colors.white),
                      ),
                      Text(
                        "${goldWrap.gold.ref?.sell ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
                    child: Text(
                      "ราคาทองรูปพรรณ",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: TextSizeMedium,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      getIconStatusGold(goldWrap.gold.jewel?.status ?? ''),
                      SizedBox(
                        width: 4.w,
                      ),
                      Text(
                        "${goldWrap.gold.jewel?.buyChange} บาท",
                        style: TextStyle(
                            fontSize: TextSizeMedium,
                            color: getColorStatusGold(goldWrap.gold.jewel?.status ?? ''),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาซื้อ(บาทละ)",
                        style: TextStyle(
                            color: Colors.white, fontSize: TextSizeNormal),
                      ),
                      Text(
                        "${goldWrap.gold.jewel?.buy ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "ราคาขาย(บาทละ)",
                        style: TextStyle(
                            color: Colors.white, fontSize: TextSizeNormal),
                      ),
                      Text(
                        "${goldWrap.gold.jewel?.sell ?? '-'}",
                        style: TextStyle(
                            color: const Color(0xff53fdd7),
                            fontSize: TextSizeMedium,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
