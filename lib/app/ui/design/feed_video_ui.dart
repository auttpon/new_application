import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:news_application/app/controller/feed_video_controller.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/ui/design/tv_online_ui.dart';
import 'package:news_application/app/ui/design/video_content_ui.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/referent_link.dart';
import 'package:news_application/app/ui/widget/title_video.dart';

class FeedVideoUI extends GetView<FeedVideoController> {
  static const _gridDelegate = SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2, childAspectRatio: 1);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        // floatingActionButton: Obx(()=> Visibility(
        //   visible: controller.isVisibleFab,
        //   child: FloatingActionButton.extended(
        //     // backgroundColor: const Color(0xff03dac6),
        //     foregroundColor: Colors.black,
        //     onPressed: () {
        //       TvOnlineUI.open();
        //     },
        //     icon: Icon(Icons.live_tv, color: Colors.white,),
        //     label: Text('Tv Online', style: TextStyle(color: Colors.white, fontSize: TextSizeNormal),),
        //   ),
        // )),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(ScreenUtil().statusBarHeight + 8.w),
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16.w,
              ),
              child: Row(
                children: [
                  Icon(Icons.slow_motion_video_outlined),
                  SizedBox(
                    width: 8.w,
                  ),
                  Center(
                    child: Text(
                      "วิดีโอ",
                      style: TextStyle(
                          fontSize: TextSizeMedium,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.getFeedVideo();
          },
          child: Obx(() => CustomScrollView(
                physics: const BouncingScrollPhysics(),
                controller: controller.scrollController,
                slivers: [
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.panorama[index];
                      return InkWell(
                        onTap: () {
                          VideoContentUI.open('video', video.toJson());
                        },
                        child: TitleVideo(
                          id: video.id,
                          image: video.image,
                          title: video.title,
                        ),
                      );
                    }, childCount: controller.panorama.length),
                  ),
                  SliverVisibility(
                    visible: controller.news.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "วิดีโอใหม่",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.news[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.news.length),
                  ),
                  SliverVisibility(
                    visible: controller.interview.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "Interview | Talk",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.interview[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.interview.length),
                  ),
                  SliverVisibility(
                    visible: controller.documentary.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "สารคดี",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.documentary[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.documentary.length),
                  ),
                  SliverVisibility(
                    visible: controller.ent.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "ความบันเทิง",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.ent[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.ent.length),
                  ),
                  SliverVisibility(
                    visible: controller.variety.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "Variety",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.variety[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.variety.length),
                  ),
                  SliverVisibility(
                    visible: controller.business.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "ธุรกิจ",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.business[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.business.length),
                  ),
                  SliverVisibility(
                    visible: controller.auto.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                        child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      child: Text(
                        "Auto",
                        style: TextStyle(
                            fontSize: TextSizeNormal,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                  SliverGrid(
                    gridDelegate: _gridDelegate,
                    delegate: SliverChildBuilderDelegate((context, index) {
                      Video video = controller.auto[index];
                      return InkWell(
                          onTap: () {
                            VideoContentUI.open('video', video.toJson());
                          },
                          child: TitleVideo(
                            id: video.id,
                            image: video.image,
                            title: video.title,
                          ));
                    }, childCount: controller.auto.length),
                  ),
                  SliverVisibility(
                    visible: controller.originalLink.isNotEmpty,
                    sliver: SliverToBoxAdapter(
                      child: ReferentLink(controller.originalLink),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
