import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/controller/tv_online_controller.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/title_video.dart';

class TvOnlineUI extends GetView<TvOnlineController> {
  static void open() {
    Get.toNamed(Routes.FEED_TV, preventDuplicates: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tv Online',
          style: TextStyle(fontSize: TextSizeBig, fontWeight: FontWeight.bold),
        ),
      ),
      body: Obx(() => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: controller.chewieController != null &&
                        controller.chewieController!.videoPlayerController.value
                            .isInitialized
                    ? AspectRatio(
                        aspectRatio: controller.aspectRatio,
                        child: Chewie(
                          controller: controller.chewieController!,
                        ),
                      )
                    : Container(
                        height: 0.5625.sw,
                        child: Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: FadeInImage.assetNetwork(
                              placeholder: "assets/icon/app_logo_2.png",
                              image: '${controller.feedTv?.image ?? ''}',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
              ),
              // SliverToBoxAdapter(
              //   child: controller.isLoadAdsBanner ? Padding(
              //     padding: EdgeInsets.all(8.w),
              //     child: Container(
              //       width: controller.bannerAd.size.width.toDouble(),
              //       height: controller.bannerAd.size.height.toDouble(),
              //       child: AdWidget(
              //         ad: controller.bannerAd,
              //       ),
              //     ),
              //   ) : SizedBox.shrink(),
              // ),
              Padding(
                padding: EdgeInsets.all(8.w),
                child: Text(
                  'รายการแนะนำ',
                  style: TextStyle(fontSize: TextSizeMedium),
                ),
              ),
              Expanded(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 1),
                  itemBuilder: (context, index) {
                    var video = controller.lastNews[index];
                    return InkWell(
                      onTap: () {
                        controller.clickVideo(index);
                      },
                      child: TitleVideo(
                        id: video.id,
                        title: video.title,
                        image: video.image,
                      ),
                    );
                  },
                  itemCount: controller.lastNews.length,
                ),
              )
              //scrolling
            ],
          )),
    );
  }
}
