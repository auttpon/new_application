import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/controller/news_paper_controller.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/design/photo_view_ui.dart';
import 'package:news_application/app/ui/design/read_news_ui.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/secondary_card.dart';

class NewsPaperUI extends GetView<NewsPaperController> {
  static void open() {
    Get.toNamed(Routes.NEWS_PAPER);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ข่าวหน้าหนึ่ง | หนังสือพิมพ์',
          style: TextStyle(fontSize: TextSizeBig, fontWeight: FontWeight.bold),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.getFeedNewsPaper();
        },
        child: Obx(()=> CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverToBoxAdapter(
              child: InkWell(
                onTap: (){
                  var src = controller.paper;
                  Get.to(PhotoViewUI(tag: src, src: src,title: 'ข่าวหน้าหนึ่ง | หนังสือพิมพ์',));
                },
                child: Hero(
                  tag: '${controller.paper}',
                  child: Image.network(
                    controller.paper,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            // SliverToBoxAdapter(
            //   child: controller.isLoadAdsBanner ? Padding(
            //     padding: EdgeInsets.all(8.w),
            //     child: Container(
            //       width: controller.bannerAd.size.width.toDouble(),
            //       height: controller.bannerAd.size.height.toDouble(),
            //       child: AdWidget(
            //         ad: controller.bannerAd,
            //       ),
            //     ),
            //   ) : SizedBox.shrink(),
            // ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                    (context, index) {
                  final column = controller.column[index];
                  var modify = column..publishTimeTh = column.publishTime;
                  return InkWell(
                    onTap: () {
                      ReadNewsUI.open(modify);
                    },
                    child: Container(
                        width: double.infinity,
                        height: 150.w,
                        margin: EdgeInsets.symmetric(horizontal: 8.w),
                        child: SecondaryCard(modify)),
                  );
                },
                childCount: controller.column.length,
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
              sliver: SliverToBoxAdapter(
                child: Text(
                  "คอลัมน์หนังสือพิมพ์วันนี้",
                  style: TextStyle(
                      fontSize: TextSizeNormal,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                    (context, index) {
                  final lastNews = controller.lastNews[index];
                  var modify = lastNews..publishTimeTh = lastNews.publishTime;
                  return InkWell(
                    onTap: () {
                      ReadNewsUI.open(modify);
                    },
                    child: Container(
                        width: double.infinity,
                        height: 150.w,
                        margin: EdgeInsets.symmetric(horizontal: 8.w),
                        child: SecondaryCard(modify)),
                  );
                },
                childCount: controller.lastNews.length,
              ),
            ),
          ],
        )),
      ),
    );
  }
}
