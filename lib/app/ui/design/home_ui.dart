import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/controller/home_controller.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/routes/app_routes.dart';
import 'package:news_application/app/ui/design/read_news_ui.dart';
import 'package:news_application/app/ui/design/video_content_ui.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:news_application/app/ui/widget/news_column_card.dart';
import 'package:news_application/app/ui/widget/news_last_card.dart';
import 'package:news_application/app/ui/widget/primary_card.dart';
import 'package:news_application/app/ui/widget/referent_link.dart';
import 'package:news_application/app/ui/widget/secondary_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:news_application/app/ui/widget/title_video.dart';
// import 'package:native_admob_flutter/native_admob_flutter.dart' as ads;

class HomeUI extends GetView<HomeController> {
  ScrollController _scrollController = ScrollController();
  var _selectedIndex = 0.obs;

  final _tabs = [
    Tab(
      text: "ทั่วไทย",
      key: Key('1'),
    ),
    Tab(text: "เศรษฐกิจ", key: Key('2')),
    Tab(text: "ต่างประเทศ", key: Key('3')),
    Tab(text: "การเมือง", key: Key('4')),
    Tab(text: "สังคม", key: Key('5')),
    Tab(text: "อาชญากรรม", key: Key('6')),
    Tab(text: "Tech", key: Key('7')),
  ];

  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      child: DefaultTabController(
        length: _tabs.length,
        initialIndex: _selectedIndex.value,
        child: SafeArea(
          bottom: false,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(ScreenUtil().statusBarHeight),
              child: Container(
                color: Colors.white,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: TabBar(
                    labelColor: Colors.pink,
                    unselectedLabelColor: Colors.black87,
                    // unselectedLabelStyle: TextStyle(),
                    indicatorSize: TabBarIndicatorSize.label,
                    isScrollable: true,
                    indicatorColor: Colors.white,
                    labelStyle: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: TextSizeNormal),
                    tabs: _tabs,
                    onTap: (index) async {
                      _scrollController.animateTo(
                        0.0,
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 300),
                      );
                      _selectedIndex.value = index;
                      controller.getLoadNews(Category.values[index]);
                    },
                  ),
                ),
              ),
            ),
            //PageStorage
            body: RefreshIndicator(
              onRefresh: () async {
                controller.refreshNews;
              },
              child: Container(
                color: Colors.white,
                child: Obx(() => CustomScrollView(
                      physics: const BouncingScrollPhysics(),
                      controller: _scrollController,
                      slivers: [
                        SliverVisibility(
                          visible: controller.panorama.isNotEmpty,
                          sliver: SliverToBoxAdapter(
                            child: Container(
                              height: 270.w, //width * 2 / 3,
                              child: PageView.builder(
                                controller:
                                    PageController(viewportFraction: 0.9),
                                physics: const BouncingScrollPhysics(),
                                itemBuilder: (context, index) {
                                  final panorama = controller.panorama[index];
                                  return InkWell(
                                    onTap: () {
                                      ReadNewsUI.open(panorama);
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(right: 4.w),
                                        child: PrimaryCard(panorama)),
                                  );
                                },
                                scrollDirection: Axis.horizontal,
                                itemCount: controller.panorama.length,
                              ),
                            ),
                          ),
                        ),
                        // SliverToBoxAdapter(
                        //   child: controller.isLoadAdsBanner ? Padding(
                        //     padding: EdgeInsets.all(8.w),
                        //     child: Container(
                        //       width: controller.bannerAd.size.width.toDouble(),
                        //       height: controller.bannerAd.size.height.toDouble(),
                        //       child: AdWidget(
                        //         ad: controller.bannerAd,
                        //       ),
                        //     ),
                        //   ) : SizedBox.shrink(),
                        // ),
                        SliverVisibility(
                          visible: controller.highlight.isNotEmpty,
                          sliver: SliverToBoxAdapter(
                              child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.w, vertical: 8.w),
                            child: Text(
                              "ไฮไลต์",
                              style: TextStyle(
                                  fontSize: TextSizeNormal,
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                        ),
                        // ads.NativeAd(
                        //   controller: controller.controllerNativeAds,
                        //   height: 60,
                        //   builder: (context, child) {
                        //     return Material(
                        //       elevation: 8,
                        //       child: child,
                        //     );
                        //   },
                        //   buildLayout: ads.adBannerLayoutBuilder,
                        //   loading: Text('loading'),
                        //   error: Text('error'),
                        //   icon: ads.AdImageView(padding: EdgeInsets.only(left: 6)),
                        //   headline: ads.AdTextView(style: TextStyle(color: Colors.black)),
                        //   advertiser: ads.AdTextView(style: TextStyle(color: Colors.black)),
                        //   body: ads.AdTextView(style: TextStyle(color: Colors.black)),
                        //   media: ads.AdMediaView(height: 70, width: 120),
                        //   button: ads.AdButtonView(
                        //     margin: EdgeInsets.only(left: 6, right: 6),
                        //     textStyle: TextStyle(color: Colors.green, fontSize: 14),
                        //     elevation: 18,
                        //     elevationColor: Colors.amber,
                        //   ),
                        // ),
                        SliverVisibility(sliver:
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                                (context, index) {
                              final highlight = controller.highlight[index];
                              return InkWell(
                                onTap: () {
                                  ReadNewsUI.open(highlight);
                                },
                                child: Container(
                                    width: double.infinity,
                                    height: 150.w,
                                    margin:
                                    EdgeInsets.symmetric(horizontal: 8.w),
                                    child: SecondaryCard(highlight)),
                              );
                            },
                            childCount: controller.highlight.length,
                          ),
                        ),visible: controller.highlight.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 8.w),
                              child: Text(
                                "ข่าวล่าสุด",
                                style: TextStyle(
                                    fontSize: TextSizeNormal,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),visible: controller.lastNews.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 8.w, vertical: 4.w),
                            height: 100.w,
                            child: PageView.builder(
                              physics: const BouncingScrollPhysics(),
                              controller:
                              PageController(viewportFraction: 0.92),
                              itemBuilder: (context, index) {
                                final lastNews = controller.lastNews[index];
                                return InkWell(
                                    onTap: () {
                                      ReadNewsUI.open(lastNews);
                                    },
                                    child: NewsLastCard(lastNews));
                              },
                              // scrollDirection: Axis.horizontal,
                              itemCount: controller.lastNews.length,
                            ),
                          ),
                        ),visible: controller.lastNews.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 8.w),
                              child: Text(
                                "ข่าวยอดนิยม",
                                style: TextStyle(
                                    fontSize: TextSizeNormal,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),visible: controller.popular.isNotEmpty,),
                        SliverVisibility(sliver: SliverList(
                          delegate:
                          SliverChildBuilderDelegate((context, index) {
                            final popular = controller.popular[index];
                            return InkWell(
                              onTap: () {
                                ReadNewsUI.open(popular);
                              },
                              child: Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 8.w, vertical: 4.w),
                                  child: NewsColumnCard(popular)),
                            );
                          }, childCount: controller.popular.length),
                        ),visible: controller.popular.isNotEmpty,),
                        SliverVisibility(
                          visible: controller.video.isNotEmpty,
                          sliver: SliverToBoxAdapter(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16.w, vertical: 8.w),
                                child: Text(
                                  "วิดีโอ",
                                  style: TextStyle(
                                      fontSize: TextSizeNormal,
                                      fontWeight: FontWeight.bold),
                                ),
                              )),
                        ),
                        SliverVisibility(sliver: SliverGrid(
                          gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2, childAspectRatio: 1),
                          delegate:
                          SliverChildBuilderDelegate((context, index) {
                            final video = controller.video[index];
                            return InkWell(
                                onTap: () async {
                                  VideoContentUI.open('news', video.toJson());
                                },
                                child: TitleVideo(
                                  id: video.id,
                                  title: video.title,
                                  image: video.image,
                                )
                            );
                          }, childCount: controller.video.length),
                        ),visible: controller.video.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 8.w),
                              child: Text(
                                "อื่นๆ",
                                style: TextStyle(
                                    fontSize: TextSizeNormal,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),visible: controller.breakingNews.isNotEmpty,),
                        SliverVisibility(sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                                (context, index) {
                              final breakingNews =
                              controller.breakingNews[index];
                              return InkWell(
                                onTap: () {
                                  ReadNewsUI.open(breakingNews);
                                },
                                child: Container(
                                    width: double.infinity,
                                    height: 150.w,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 8.w, vertical: 4.w),
                                    child: SecondaryCard(breakingNews)),
                              );
                            },
                            childCount: controller.breakingNews.length,
                          ),
                        ),visible: controller.breakingNews.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 8.w),
                              child: Text(
                                "คอลัมน์",
                                style: TextStyle(
                                    fontSize: TextSizeNormal,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),visible: controller.column.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                          child: Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 4.0),
                            height: 150.w,
                            child: PageView.builder(
                              physics: const BouncingScrollPhysics(),
                              controller:
                              PageController(viewportFraction: 0.93),
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    ReadNewsUI.open(controller.column[index]);
                                  },
                                  child:
                                  SecondaryCard(controller.column[index]),
                                );
                              },
                              // scrollDirection: Axis.horizontal,
                              itemCount: controller.column.length,
                            ),
                          ),
                        ),visible: controller.column.isNotEmpty,),
                        SliverVisibility(sliver: SliverToBoxAdapter(
                          child:
                          ReferentLink('${controller.originalLink.value}'),
                        ),visible: controller.originalLink.isNotEmpty,)
                      ],
                    )),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
