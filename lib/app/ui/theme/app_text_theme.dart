
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


final TextStyle textHeader = TextStyle( fontSize: 20, fontWeight: FontWeight.normal);
final TextStyle textTitle = TextStyle( fontSize: 18, fontWeight: FontWeight.normal);
final TextStyle textBody = TextStyle( fontSize: 16, fontWeight: FontWeight.normal);
final TextStyle textSmall = TextStyle( fontSize: 14, fontWeight: FontWeight.normal);

double TextSizeSmall = 12.sp;
double TextSizeNormal = 14.sp;
double TextSizeMedium = 16.sp;
double TextSizeBig = 18.sp;