import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final ThemeData appThemeData = ThemeData(
    primaryColor: Colors.white,
    // primarySwatch: Colors.red,
    // accentColor: Colors.pink,
    splashColor: Colors.pink,
    highlightColor: Colors.pink,
    textTheme: GoogleFonts.kanitTextTheme());
