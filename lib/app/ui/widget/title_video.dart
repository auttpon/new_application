import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';

class TitleVideo extends StatelessWidget {
  dynamic id;
  String? title, image;

  TitleVideo({required this.id, this.title, this.image});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = (size.width / 2) - 8;
    final height = ((size.width / 2) * 2 / 3) - 8;
    return Container(
      color: Colors.grey[200],
      margin: EdgeInsets.all(2.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Hero(
                tag: '${id}',
                child: Image.network(
                  '${image ?? ''}',
                  height: height,
                  width: width,
                  fit: BoxFit.cover,
                  loadingBuilder: (context, child, loadingProgress) {
                    if (loadingProgress == null) return child;
                    return Container(
                      color: Colors.red,
                      height: double.infinity,
                      child: Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                              : null,
                        ),
                      ),
                    );
                  },
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      'assets/icon/app_logo_1.png',
                      height: height,
                      width: width,
                    );
                  },
                ),
              ),
              Opacity(
                opacity: 0.7,
                child: Icon(
                  Icons.play_circle_fill,
                  color: Colors.white,
                  size: 30.w,
                ),
              ),
            ],
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(4.w),
              //symmetric(horizontal: 4.w, vertical: 4.w),
              child: Text(
                '${title ?? ''}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: TextSizeNormal),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
