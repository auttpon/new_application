import 'package:flutter/material.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SecondaryCard extends StatelessWidget {
  News? _news;
  SecondaryCard(this._news);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.r)),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 8.w),
        child: Row(
          children: [
            Container(
              width: 110.w,
              height: 150.h,
              // decoration: BoxDecoration(
              //   borderRadius: BorderRadius.circular(8.0),
              //   image: DecorationImage(
              //     image: NetworkImage('${_news?.image}'),
              //     fit: BoxFit.cover,
              //   ),
              // ),
              child:  Hero(
                tag: '${_news?.id}',
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.r),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/icon/app_logo_1.png",
                    image:'${_news?.image}',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(width: 8.w),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${_news?.title}',
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: TextSizeNormal),
                    ),
                    SizedBox(height: 4.h),
                    Text(
                      '${_news?.abstract}',
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                        style: TextStyle(fontSize: TextSizeNormal)
                    ),
                    Spacer(),
                    Text('${_news?.publishTimeTh}',style: TextStyle(fontSize: TextSizeNormal, color: Colors.green[600]))
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
