import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PrimaryCard extends StatelessWidget {
   News _news;
   PrimaryCard(this._news);

  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.r)),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 8.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 0.47.sw,//(width * 2 / 3) - 80,
              child: Hero(
                tag: '${_news.id}',
                child: Container(
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.r),
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/icon/app_logo_2.png",
                        image:'${_news.image!}',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 4.h),
            Text(
                '${_news.title!}',
                overflow: TextOverflow.ellipsis,
                maxLines: 2, style: TextStyle(fontSize: TextSizeNormal, fontWeight: FontWeight.bold)
            ),
            // Text(
            //   '${_news.abstract!}',
            //   overflow: TextOverflow.ellipsis,
            //   maxLines: 3, style: TextStyle(fontSize: TextSizeNormal)
            // ),
            SizedBox(height: 4.h),
            Row(
              children: [
                Icon(CupertinoIcons.time, size: 14.w,),
                SizedBox(width: 4.w,),
                Text('${_news.publishTimeTh}', style: TextStyle(fontSize: TextSizeNormal),),
                Icon(CupertinoIcons.minus, color: Colors.green[600],),
                Text('${_news.topic ?? 'unknown'}',style: TextStyle(color: Colors.green[600], fontSize: TextSizeNormal)),
              ],
            ),
          ],
        ),
      ),
    );
  }

}