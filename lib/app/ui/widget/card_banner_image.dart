import 'package:flutter/material.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CardBannerImage extends StatelessWidget {
  late String image, title;

  CardBannerImage({required this.image,required this.title});

  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    return Card(
      margin:  EdgeInsets.symmetric(
          horizontal: 8.w, vertical: 4.w),
      elevation: 3.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0)),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          // ClipRRect(
          //   borderRadius: BorderRadius.circular(8.0),
          //   child: Image.network(
          //     image,
          //     fit: BoxFit.fill,
          //   ),
          // ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.r),
              image: DecorationImage(
                image: NetworkImage('$image'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.black45,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8.r),
                    bottomRight: Radius.circular(8.r))),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.w),
              child: Text(
                title,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white, fontSize: TextSizeNormal),
              ),
            ),
          )
        ],
      ),
    );
  }

}