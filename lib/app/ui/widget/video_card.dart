import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VideoCard extends StatelessWidget {
  News video;

  VideoCard(this.video);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.r)),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Hero(
            tag: '${video.id}',
            child: Container(
              // height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                image: DecorationImage(
                  image: NetworkImage('${video.image}'),
                  fit: BoxFit.cover,
                ),
              ),
              // child: ClipRRect(
              //   borderRadius: BorderRadius.circular(8.r),
              //   child: Image.network('${video.image}', fit: BoxFit.contain,)
              //   // FadeInImage.assetNetwork(
              //   //   placeholder: "assets/icon/app_logo_2.png",
              //   //   image: '${video.image}',
              //   //   fit: BoxFit.contain,
              //   // ),
              // ),
            ),
          ),
          Align(child: Padding(
            padding: EdgeInsets.all(4.w),
            child: Icon(CupertinoIcons.play_fill, color: Colors.white,),
          ), alignment: Alignment.bottomRight,)
        ],
      ),
    );
  }
}
