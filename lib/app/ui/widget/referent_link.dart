import 'package:flutter/material.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ReferentLink extends StatelessWidget {
  final String url;
  MainAxisAlignment alignment;
  ReferentLink(this.url,{this.alignment = MainAxisAlignment.center});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: alignment,
      children: [
        Text(
          'ที่มา:',
          style: TextStyle(fontSize: TextSizeNormal, fontWeight: FontWeight.bold),
        ),
        TextButton(
          onPressed: () async {
            print(url);
            await canLaunch(url)
                ? await launch(
                    url,
                  )
                : throw 'Could not launch $url';
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                Icons.link,
                color: Colors.green[600],
              ),
              SizedBox(
                width: 4.w,
              ),
              Text(
                'ไทยรัฐออนไลน์',
                style: TextStyle(fontSize: TextSizeNormal, fontWeight: FontWeight.bold,color: Colors.green[600]),
              ),
              SizedBox(
                width: 4.w,
              ),
              Text('- ดูข่าวต้นฉบับ',
                  style: TextStyle(
                      color: Colors.lightBlue,
                      fontWeight: FontWeight.bold,
                      fontSize: TextSizeNormal)),
            ],
          ),
        )
      ],
    );
  }
}
