import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NewsLastCard extends StatelessWidget {
  News? _news;

  NewsLastCard(this._news);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.r)),
      color: Colors.white,
      child: Container(
        // width: 350.w,
        padding: EdgeInsets.all(8.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(
                  CupertinoIcons.time,
                ),
                SizedBox(
                  width: 4.w,
                ),
                Text(
                  '${_news?.publishTimeTh}',
                  style: TextStyle(fontSize: TextSizeNormal),
                ),
                Icon(
                  CupertinoIcons.minus,
                  color: Colors.green[600],
                ),
                Text('${_news?.topic}',
                    style: TextStyle(
                        color: Colors.green[600], fontSize: TextSizeNormal)),
              ],
            ),
            SizedBox(
              height: 4.h,
            ),
            Text('${_news?.title}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: TextSizeNormal)),
          ],
        ),
      ),
    );
  }
}
