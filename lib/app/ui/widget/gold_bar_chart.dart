import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:news_application/app/data/model/feed_gold.dart';


class GoldBarChart extends StatelessWidget {
  final Color leftBarColor = const Color(0xff53fdd7);
  final Color rightBarColor = const Color(0xffff5182);
  final double width = 7;

  List<GoldWrap> gold;

  GoldBarChart(this.gold);

  List<BarChartGroupData>  showingBarGroups() {
    var i = 0;
    final items = <BarChartGroupData>[];
    gold.forEach((element) {
      int  sell = element.gold.ref?.sell ?? 0;//as double;
      int  buy = element.gold.ref?.buy ?? 0; //as double;
      debugPrint("grap: ${element.date}");
      items.add(makeGroupData(i, sell.toDouble(), buy.toDouble()));
      i++;
    });
    return items;
  }

  List<String> get _dateList => gold.map((e) {
    // var d = _dateFormat.parse('25 Mar 2021');
    // print(d);
    var s = e.date.split(' ');
    return '${s[0]}\n${s[1]}';
  }).toList();



  // late int touchedGroupIndex;
  // late List<BarChartGroupData> rawBarGroups ;

  // @override
  // void initState() {
  //   super.initState();
  //   final barGroup1 = makeGroupData(0, 5, 12);
  //   final barGroup2 = makeGroupData(1, 16, 12);
  //   final barGroup3 = makeGroupData(2, 18, 5);
  //   final barGroup4 = makeGroupData(3, 20, 16);
  //   final barGroup5 = makeGroupData(4, 17, 6);
  //   final barGroup6 = makeGroupData(5, 19, 1.5);
  //   final barGroup7 = makeGroupData(6, 10, 1.5);
  //
  //   final items = [
  //     barGroup1,
  //     barGroup2,
  //     barGroup3,
  //     barGroup4,
  //     barGroup5,
  //     barGroup6,
  //     barGroup7,
  //   ];
  //
  //   rawBarGroups = items;
  //
  //   showingBarGroups = rawBarGroups;
  // }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        color: const Color(0xff2c4260),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  makeTransactionsIcon(),
                  const SizedBox(
                    width: 38,
                  ),
                  const Text(
                    'กราฟทองแท่ง',
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  const Text(
                    'state',
                    style: TextStyle(color: Color(0xff77839a), fontSize: 16),
                  ),
                ],
              ),
              const SizedBox(
                height: 38,
              ),
              // Expanded(
              //   child: Padding(
              //     padding: const EdgeInsets.symmetric(horizontal: 8.0),
              //     child: BarChart(
              //       BarChartData(
              //         maxY: 40000,//20
              //         minY: 15000,
              //         titlesData: FlTitlesData(
              //           show: true,
              //           bottomTitles: SideTitles(
              //             showTitles: true,
              //             getTextStyles: (value) => const TextStyle(
              //                 color: Color(0xff7589a2), fontWeight: FontWeight.bold, fontSize: 14),
              //             margin: 20,
              //             getTitles: (double value) {
              //               return _dateList[value.toInt()];
              //             },
              //           ),
              //           leftTitles: SideTitles(
              //             showTitles: true,
              //             getTextStyles: (value) => const TextStyle(
              //                 color: Color(0xff7589a2), fontWeight: FontWeight.bold, fontSize: 14),
              //             margin: 32,
              //             reservedSize: 14,
              //             getTitles: (value) {
              //               if (value == 15000) {
              //                 return '15K';
              //               }else if (value == 20000) {
              //                 return '20K';
              //               } else if (value == 25000) {
              //                 return '25K';
              //               }else if (value == 30000) {
              //                 return '30K';
              //               }else if (value == 35000) {
              //                 return '35K';
              //               } else if (value == 40000) {
              //                 return '40K';
              //               }
              //               else {
              //                 return '';
              //               }
              //             },
              //           ),
              //         ),
              //         borderData: FlBorderData(
              //           show: false,
              //         ),
              //         barGroups: showingBarGroups(),
              //       ),
              //     ),
              //   ),
              // ),
              const SizedBox(
                height: 12,
              ),
            ],
          ),
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(int x, double y1, double y2) {
    debugPrint("$y1,,$y2");
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        toY: y1,
        // color: leftBarColor,
        width: width,
      ),
      BarChartRodData(
        toY: y2,
        // colors: [rightBarColor],
        width: width,
      ),
    ]);
  }

  Widget makeTransactionsIcon() {
    const double width = 4.5;
    const double space = 3.5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 42,
          color: Colors.white.withOpacity(1),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
      ],
    );
  }

}