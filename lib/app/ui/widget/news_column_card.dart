import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/ui/theme/app_text_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NewsColumnCard extends StatelessWidget {
  News? _news;
  NewsColumnCard(this._news);
  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.r)),
      color: Colors.white,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                height: 0.6.sw,
                child: Hero(
                  tag: '${_news?.id}',
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(8.r),
                        topLeft: Radius.circular(8.r)),
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/icon/app_logo_2.png",
                      image:
                      '${_news?.image}',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 4.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 8.w, vertical: 4.w),
              child: Row(
                children: [
                  Icon(
                    CupertinoIcons.time,
                    size: 14.w,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  Text(
                    '${_news?.publishTimeTh}',
                    style: TextStyle(fontSize: TextSizeNormal),
                  ),
                  Icon(
                    CupertinoIcons.minus,
                    color: Colors.green[600],
                  ),
                  Text('${_news?.topic}',
                      style: TextStyle(
                          color: Colors.green[600],
                          fontSize: TextSizeNormal)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 8.w, vertical: 4.w),
              child: Text(
                '${_news?.title}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: TextSizeNormal,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 8.w, vertical: 4.w),
              child: Text(
                '${_news?.abstract}',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: TextSizeNormal,),
              ),
            ),
          ],
        ),
      ),
    );
  }

}