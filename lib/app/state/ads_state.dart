
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/util/id_mobile_ads.dart';

mixin AdsState on GetxController {
  RxBool _isLoadAdsBanner = false.obs;
  bool get isLoadAdsBanner => _isLoadAdsBanner.value;

  // late BannerAd bannerAd;

  @override
  void onReady() {
    _instantiateBanner();
    super.onReady();
  }

  _instantiateBanner(){
    // bannerAd = BannerAd(
    //   adUnitId: IdMobileAds.bannerAdUnitId,
    //   size: AdSize.largeBanner,
    //   request: AdRequest(
    //       testDevices: IdMobileAds.TestDevices
    //   ),
    //   listener: AdListener(onAdLoaded: (_){
    //     _isLoadAdsBanner.value = true;
    //   },onAdFailedToLoad: (ad,error){
    //     print('error admob: ${error.message}');
    //     ad.dispose();
    //     _isLoadAdsBanner.value = false;
    //   }),
    // );
    // bannerAd.load();
  }

  @override
  void onClose() {
    // bannerAd.dispose();
    super.onClose();
  }

}
