import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';


mixin LoadingState on GetxController {

  showLoading([String statusLoading = 'Loading...']) async {
    await EasyLoading.show(status: statusLoading);
    debugPrint("show loading");
  }

  hideLoading() async {
    await EasyLoading.dismiss();
    debugPrint("hide loading");
  }

  showErrorMessage(message) async {
    await EasyLoading.showError(message);
  }


  @override
  void onClose() {
    hideLoading();
    super.onClose();
  }

}