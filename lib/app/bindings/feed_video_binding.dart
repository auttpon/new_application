import 'package:get/get.dart';
import 'package:news_application/app/controller/feed_video_controller.dart';

class FeedVideoBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FeedVideoController());
  }

}