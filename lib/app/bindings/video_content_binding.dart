import 'package:get/get.dart';
import 'package:news_application/app/controller/video_content_controller.dart';

class VideoContentBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => VideoContentController());
  }

}