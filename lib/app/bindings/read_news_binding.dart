import 'package:get/get.dart';
import 'package:news_application/app/controller/read_news_controller.dart';

class ReadNewsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ReadNewsController());
  }

}