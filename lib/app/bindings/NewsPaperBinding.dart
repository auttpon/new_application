import 'package:get/get.dart';
import 'package:news_application/app/controller/news_paper_controller.dart';

class NewsPaperBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NewsPaperController());
  }

}