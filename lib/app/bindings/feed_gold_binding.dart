import 'package:get/get.dart';
import 'package:news_application/app/controller/feed_gold_controller.dart';

class FeedGoldBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FeedGoldController());
  }

}