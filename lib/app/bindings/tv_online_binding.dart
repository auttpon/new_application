import 'package:get/get.dart';
import 'package:news_application/app/controller/tv_online_controller.dart';

class TvOnlineBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TvOnlineController());
  }

}