import 'package:async/async.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:news_application/app/data/model/feed_tv.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/ads_state.dart';
import 'package:news_application/app/state/loading_state.dart';
import 'package:news_application/app/ui/design/video_content_ui.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart' as yt;

class TvOnlineController extends GetxController with AdsState, LoadingState {
  RestClient _client = RestClient.to;
  static const _BASE_URL = 'https://www.thairath.co.th/tv/live';
  var _yt = yt.YoutubeExplode();

  VideoPlayerController? _videoPlayerController;
  Rx<ChewieController?> _rxcChewieController = Rx(null);
  CancelableOperation<FeedTv?>? cancellableOperation;

  Rx<FeedTv?> _feedTv = Rx(null);

  ChewieController? get chewieController => _rxcChewieController.value;

  double get aspectRatio => _videoPlayerController?.value.aspectRatio ?? 0.0;

  FeedTv? get feedTv => _feedTv.value;

  List<Video> get lastNews => _feedTv.value?.lastNews ?? [];

  _getFeedTv() async {
    // cancellableOperation?.cancel();
    cancellableOperation =
        CancelableOperation.fromFuture(_client.getFeedTv(_BASE_URL));
    var feedTv = await cancellableOperation?.value;
    _feedTv.value = feedTv;
    if (feedTv?.sourceVideo != null) {
      initializePlayer(Uri.parse(feedTv?.sourceVideo ?? ""));
    }
  }

  void clickVideo(int position) async {
    _stopPlayer();
    showLoading('Loading... video info');
    var video = lastNews[position];
    if (video.playList?.isNotEmpty == true) {
      var playlist = video.playList?.first;
      if (playlist?.youtubeId != null) {
        var manifest = await _yt.videos.streamsClient
            .getManifest('${playlist?.youtubeId.toString()}');

        var videoOnly = manifest.muxed
        // .where((element) => element.container == yt.StreamContainer.mp4 && element.videoQuality == yt.VideoQuality.high720)
         .withHighestBitrate();

        // manifest.muxed.forEach((element) {
        //   print('muxed: ${element.toString()}');
        // });
        // var b = manifest.muxed.withHighestBitrate();

        var json = Video()
        ..source = videoOnly.url.toString()
        ..title = video.title
        ..abstract = video.abstract
        ..canonical = video.canonical
        ;
        VideoContentUI.open('video', json.toJson());
        print('url: ${videoOnly.url}');
      }
    }
    hideLoading();
  }

  _stopPlayer() {
    // chewieController?.dispose();
    _videoPlayerController?.pause();
    _videoPlayerController?.seekTo(const Duration());
  }

  Future<void> initializePlayer(Uri url) async {
    showLoading('streaming content video');
    _videoPlayerController = VideoPlayerController.networkUrl(url);
    await Future.wait([_videoPlayerController!.initialize()]);
    _rxcChewieController.value = ChewieController(
      videoPlayerController: _videoPlayerController!,
      autoPlay: true,
      looping: false,
      showControls: true,
      materialProgressColors: ChewieProgressColors(
        playedColor: Colors.pink,
        handleColor: Colors.pink,
        backgroundColor: Colors.grey,
        bufferedColor: Color(0xFFF06292),
      ),
      placeholder: Container(
        color: Colors.grey,
      ),
      autoInitialize: true,
    );
    hideLoading();
  }

  @override
  void onReady() {
    _getFeedTv();
    super.onReady();
  }

  @override
  void onClose() async {
    _yt.close();
    cancellableOperation?.cancel();
    _videoPlayerController?.dispose();
    chewieController?.dispose();
    super.onClose();
  }
}
