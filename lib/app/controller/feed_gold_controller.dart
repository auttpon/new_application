import 'package:get/get.dart';
import 'package:news_application/app/state/ads_state.dart';
import 'package:news_application/app/data/model/feed_gold.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/loading_state.dart';

class FeedGoldController extends GetxController with AdsState, LoadingState {
  RestClient _client = RestClient.to;
  static const _BASE_URL = 'https://www.thairath.co.th/news/gold';

  Rx<FeedGold?> _feedGold = Rx(null);
  RxString _originalLink = "".obs;

  String get originalLink => _originalLink.value; //ticket visible view
  GoldWrap? get priceToDay {
    var data = _feedGold.value?.goldPriceToDay ?? [];
    return data.isNotEmpty ? data.first : null;
  }
  List<News> get news {
    return _feedGold.value?.last ?? [];
  }

  List<GoldWrap> get graphs => _feedGold.value?.graphs ?? [];
  List<GoldWrap> get listGolds => _feedGold.value?.listGolds ?? [];

  getFeedGold() async {
    showLoading();
    FeedGold? feedGold = await _client.getFeedGold(_BASE_URL);
    _feedGold.value = feedGold;
    _originalLink.value = _BASE_URL;
    hideLoading();
  }

@override
  void onReady() {
    getFeedGold();
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}