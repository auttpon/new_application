import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
// import 'package:native_admob_flutter/native_admob_flutter.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/ads_state.dart';
import 'package:news_application/app/state/loading_state.dart';

class HomeController extends GetxController with AdsState, LoadingState {
  RestClient _client = RestClient.to;
  Rx<NewsColumn?> _rxNewsColumn = Rx(null);
  RxString originalLink = "".obs;
  Category? _category;
  // final controllerNativeAds = NativeAdController();

  StreamSubscription<ConnectivityResult>? subscriptionConnectivity;

  NewsColumn? get _newColumn => _rxNewsColumn.value;
  final _lst = <News>[];

  List<News> get panorama =>
      _newColumn?.props?.initialState?.common?.data?.items?.panorama
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get highlight =>
      _newColumn?.props?.initialState?.common?.data?.items?.highlight
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get scoop =>
      _newColumn?.props?.initialState?.common?.data?.items?.scoop
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get lastNews =>
      _newColumn?.props?.initialState?.common?.data?.items?.lastNews
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get breakingNews =>
      _newColumn?.props?.initialState?.common?.data?.items?.breakingNews
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get popular =>
      _newColumn?.props?.initialState?.common?.data?.items?.popular
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get video =>
      _newColumn?.props?.initialState?.common?.data?.items?.video
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  List<News> get column =>
      _newColumn?.props?.initialState?.common?.data?.items?.column
          ?.where((element) => element.id != null)
          .toList() ??
      _lst;

  @override
  void onInit() {
    // controllerNativeAds.load();
    super.onInit();
  }
  @override
  void onReady() async {
    subscriptionConnectivity = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      if (result == ConnectivityResult.none) {
        await EasyLoading.showInfo("ไม่มีการเชื่อมต่ออินเตอร์เน็ต");
      } else {}
    });
    getLoadNews(Category.Local);

    super.onReady();
  }

  @override
  void onClose() {
    // subscriptionConnectivity?.cancel();
    super.onClose();
  }

  Future<void> getLoadNews(Category category) async {
    _category = category;
    showLoading();
    try {
      NewsColumn? newsColumn = await _client.getColumns(category);
      if (newsColumn != null) {
        _rxNewsColumn.value = newsColumn;
      } else {
        _rxNewsColumn.value = null;
        showErrorMessage('เกิดข้อผิดพลาดโปรดรองใหม่อีกครั้ง!');
      }
      originalLink.value = category.pathUrl;
    } on SocketException {
      showErrorMessage('เกิดข้อผิดพลาดโปรดรองใหม่อีกครั้ง!');
      return;
    }
    hideLoading();
  }

  // void refreshData() async {
  //   final category = _category;
  //   if(category != null){
  //     await _getLoadNews(category);
  //   }
  // }

  void get refreshNews async =>
      getLoadNews(_category != null ? _category! : Category.Local);
}
