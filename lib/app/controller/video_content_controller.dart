import 'package:async/async.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:news_application/app/state/ads_state.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/loading_state.dart';
import 'package:video_player/video_player.dart';

class VideoContentController extends GetxController with AdsState, LoadingState {
  RestClient _client = RestClient.to; //Get.find<RestClient>();

  VideoPlayerController? _videoPlayerController;
  Rx<ChewieController?> _rxcChewieController = Rx(null);
  RxList<Video> _rxRelate = RxList();

  CancelableOperation<VideoContent?>? cancellableOperation;

  ChewieController? get chewieController => _rxcChewieController.value;

  get aspectRatio => _videoPlayerController?.value.aspectRatio ?? 0.0;

  List<Video> get relate => _rxRelate;

  Rx<News?> _news = Rx(null);

  News? get news => _news.value;

  @override
  void onReady() async {
    Map<String, dynamic> arguments = Get.arguments;
    if (arguments.containsKey('news')) {
      final _news = News.fromJson(arguments['news']);
      getVideoContent(_news);
    } else if (arguments.containsKey('video')) {
      _stopPlayer();
      final _video = Video.fromJson(arguments['video']);
      _news.value = _video;
      await initializePlayer(Uri.parse(_video.sourceVideo ?? ""));
      // News(id: _video.id, title: _video.title, abstract: _video.abstract, canonical: _video.canonical);
    } else {
      throw 'Could not get arguments :${arguments.toString()}';
    }

    super.onReady();
  }

  Future<void> getVideoContent(News news) async {
    _news.value = news;

    _stopPlayer();

    showLoading();
    // VideoContent? videoContent = await _client.getVideoContent(news.canonical ?? "");
    cancellableOperation?.cancel();
    cancellableOperation = CancelableOperation.fromFuture(
        _client.getVideoContent(news.canonical ?? ""));
    VideoContent? videoContent = await cancellableOperation?.value;

    _rxRelate.assignAll(videoContent?.relate ?? []);
    if (videoContent?.content?.isNotEmpty == true) {
      await initializePlayer(Uri.parse(videoContent?.content?.first.sourceVideo ?? ""));
    }
    hideLoading();
  }

  _stopPlayer() {
    chewieController?.dispose();
    _videoPlayerController?.pause();
    _videoPlayerController?.seekTo(const Duration());
  }

  Future<void> initializePlayer(Uri url) async {
    showLoading('streaming content video');
    _videoPlayerController = VideoPlayerController.networkUrl(url);
    await Future.wait([_videoPlayerController!.initialize()]);
    _rxcChewieController.value = ChewieController(
      videoPlayerController: _videoPlayerController!,
      autoPlay: true,
      looping: false,
      showControls: true,
      materialProgressColors: ChewieProgressColors(
        playedColor: Colors.pink,
        handleColor: Colors.pink,
        backgroundColor: Colors.grey,
        bufferedColor: Color(0xFFF06292),
      ),
      placeholder: Container(
        color: Colors.grey,
      ),
      autoInitialize: true,
    );
    hideLoading();
  }

  @override
  void onClose() async {
    cancellableOperation?.cancel();
    _videoPlayerController?.dispose();
    chewieController?.dispose();
    super.onClose();
  }
}
