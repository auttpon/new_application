import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:news_application/app/data/model/news_column.dart';
import 'package:news_application/app/data/model/news_story.dart';
import 'package:news_application/app/data/model/read_news.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/data/preference.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/ads_state.dart';
import 'package:news_application/app/state/loading_state.dart';
import 'package:news_application/app/util/id_mobile_ads.dart';
import 'package:url_launcher/url_launcher.dart';

class ReadNewsController extends GetxController with AdsState, LoadingState {
  RestClient _client = RestClient.to;
  final _pref = Preference.to;

  Rx<News?> _news = Rx(null);
  RxList<NewsRelates> _newsRelates = RxList();

  RxList<NewsStoryState> _rxNewsStory = RxList();

  var _scaleFont = 0.0.obs;

  double get scaleFont => _scaleFont.value;

  setFontSize() {
    var size = scaleFont + 2.0;
    if (size > 8.0) {
      size = 0;
    }
    _scaleFont.value = size;
    _pref.scaleFont = size;
    print('font size: $size');
  }

  News? get news => _news.value;

  List<NewsStoryState> get newsStory => _rxNewsStory;

  List<NewsRelates> get newsRelates => _newsRelates;

  // InterstitialAd? interstitialAd;

  @override
  void onInit() async {
    _scaleFont.value = _pref.scaleFont;
    super.onInit();
  }

  @override
  void onReady() async {
    //ads
    // _instantiateBanner();
    _instantiateInterstitial();

    _news.value = News.fromJson(Get.arguments['news']);
    _getReadNews('${news?.canonical}');
    super.onReady();
  }

  @override
  void onClose() {
    // bannerAd.dispose();
    // interstitialAd?.dispose();
    super.onClose();
  }

  _getReadNews(String url, [bool isReloadNewer = false]) async {
    showLoading();
    NewsStory? story = await _client.getReadNews(url);
    _rxNewsStory.value = story?.story ?? [];
    _newsRelates.value = story?.newsRelates ?? [];

    if (isReloadNewer) {
      _news.value = story?.readNews.items;
    }
    hideLoading();
  }

  clickLinkCanLaunch(String? url) async {
    debugPrint('click link $url, start with ${url?.contains('12')}');
    if (url != null && !url.contains('mailto')) {
      if (url != news?.canonical) {
        if (url.contains('https://www.thairath.co.th/')) {
          getNews(url);
        } else {
          await canLaunch(url)
              ? await launch(url)
              : throw 'Could not launch $url';
        }
      }
    }
  }

  getNews(String url) {
    _getReadNews(url, true);
  }

  Video getVideo(String src) {
    Video video = Video(source: src)
      ..id = news?.id
      ..title = news?.title
      ..image = news?.image
      ..publishTimeTh = news?.publishTimeTh
      ..abstract = news?.abstract
      ..canonical = news?.canonical;
    return video;
  }

  _instantiateInterstitial() {
    // if (_pref.isShowAds()) {
    //   interstitialAd = InterstitialAd(
    //     adUnitId: IdMobileAds.interstitialAdUnitId,
    //     request: AdRequest(testDevices: IdMobileAds.TestDevices),
    //     listener: AdListener(onAdLoaded: (_) {
    //       interstitialAd?.show();
    //     }, onAdFailedToLoad: (ad, error) {
    //       ad.dispose();
    //     }),
    //   );
    //   interstitialAd?.load();
    // }
  }
}
