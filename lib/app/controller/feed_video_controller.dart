import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:news_application/app/data/model/video_content.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/loading_state.dart';
import 'package:video_player/video_player.dart';

class FeedVideoController extends GetxController with LoadingState {
  RestClient _client = RestClient.to;

  ScrollController scrollController = ScrollController();
  RxBool _isVisibleFab = true.obs;
  bool get isVisibleFab => _isVisibleFab.value;

  static const _BASE_URL = 'https://www.thairath.co.th/video';

  Rx<FeedVideo?> _feedVideo = Rx(null);
  Rx<Video?> _watchingVideo = Rx(null);

  final _lst = <Video>[];

  List<Video> get panorama => _feedVideo.value?.panorama ?? _lst;

  List<Video> get news => _feedVideo.value?.news ?? _lst;

  List<Video> get interview => _feedVideo.value?.interview ?? _lst;

  List<Video> get documentary => _feedVideo.value?.documentary ?? _lst;

  List<Video> get ent => _feedVideo.value?.ent ?? _lst;

  List<Video> get variety => _feedVideo.value?.variety ?? _lst;

  List<Video> get business => _feedVideo.value?.business ?? _lst;

  List<Video> get auto => _feedVideo.value?.auto ?? _lst;

  Video? get watchingVideo => _watchingVideo.value;

  RxString _originalLink = "".obs;

  String get originalLink => _originalLink.value; //ticket visible view


  getFeedVideo() async {
    showLoading();
    FeedVideo? feedVideo = await _client.getFeedVideo(_BASE_URL);
    _feedVideo.value = feedVideo;
    _originalLink.value = _BASE_URL;
    hideLoading();
  }

  @override
  void onReady() {
    // _addScrollListener();
    getFeedVideo();
    super.onReady();
  }

  _addScrollListener(){
    _isVisibleFab.value = true;
    scrollController.addListener(() {
      if(scrollController.position.userScrollDirection == ScrollDirection.reverse){
        if(_isVisibleFab.value == true) { //up
          _isVisibleFab.value = false;
        }
      }else{
        if(scrollController.position.userScrollDirection == ScrollDirection.forward){
          if(_isVisibleFab.value == false) { //down
            _isVisibleFab.value = true;
          }
        }
      }
    });
  }

  @override
  void onClose() async {
    // scrollController.dispose();
    // _videoPlayerController?.dispose();
    // chewieController?.dispose();
    super.onClose();
  }
}

// VideoPlayerController? _videoPlayerController;
// Rx<ChewieController?> _chewieController = Rx();
// ChewieController? get chewieController => _chewieController.value;
// double get aspectRatio => _videoPlayerController?.value.aspectRatio ?? 0.0;

// Future<void> _initializePlayer(url) async {
//   chewieController?.dispose();
//   _videoPlayerController?.pause();
//   _videoPlayerController?.seekTo(const Duration());
//
//   _videoPlayerController = VideoPlayerController.network(url);
//   await Future.wait([_videoPlayerController!.initialize()]);
//   _chewieController.value = ChewieController(
//     videoPlayerController: _videoPlayerController!,
//     autoPlay: true,
//     looping: false,
//   );
// }