import 'package:get/get.dart';
import 'package:news_application/app/data/model/feed_news_paper.dart';
import 'package:news_application/app/data/remote/rest_client.dart';
import 'package:news_application/app/state/ads_state.dart';
import 'package:news_application/app/state/loading_state.dart';

class NewsPaperController extends GetxController with AdsState, LoadingState {
  RestClient _client = RestClient.to;
  static const _BASE_URL = 'https://www.thairath.co.th/newspaper';
  Rx<FeedNewsPaper?> _feedNewsPaper = Rx(null);

  get paper => _feedNewsPaper.value?.coverMedia ?? '';

  List<NewsPaper> get column => _feedNewsPaper.value?.items ?? [];
  List<NewsPaper> get lastNews => _feedNewsPaper.value?.latest?.items ?? [];

  Future<void> getFeedNewsPaper() async {
    showLoading();
    var feedNewsPaper = await _client.getFeedNewsPaper(_BASE_URL);
    _feedNewsPaper.value = feedNewsPaper;
    hideLoading();
  }

  @override
  void onReady() {
    getFeedNewsPaper();
    super.onReady();
  }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

}