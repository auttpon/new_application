package com.ihumn0y.news.news_application

import android.content.Context
import androidx.multidex.MultiDex
import io.flutter.app.FlutterApplication


// Created by Yob-iHumNoy. on 2/10/21 | Copyright © Yob HumNoy. All rights reserved.

class App: FlutterApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}